@extends('layout')


@section('content')
	<!--========================-->
	<!-- BASIC INFORMATION -->
	<!--========================-->
	
	<h2 class="thin">Modify Part SKU</h2>
	<p class="muted">This functionality offers modification of the Part SKU.</p>
	
	<ol class="breadcrumb text-left">
		<li><a href="{{url('/parts')}}">Parts</a></li>
		<li class="active">Edit</li>
	</ol>
	
	<hr>
	<form id="partsFormEdit" method="POST" action="{{url('/partspms/modify/'.$part->id)}}" class="text-left" enctype="multipart/form-data">
		{{ method_field('PATCH') }}
		{{ csrf_field() }}
		<div class="row">
			<div class="col-lg-6">
				<h3>{{ $part->partsku }}</h3>
				<h5><small><em>{{ $part->descr or "This text is supposed to be a description."  }}</em></small>
				<br>{{-- $part->supplierorders->existingorders->spnumber or "" --}}</h5>				
			</div>
			<div class="col-lg-6 text-right">
				<label for="photo" style="cursor: pointer;">

					<img src="{{ File::exists(public_path("storage/" . $part->photo)) == true ? "data:image/jpeg;base64," . base64_encode(Storage::get('public/' . $part->photo)) : "data:image/jpeg;base64," . base64_encode(Storage::get('public/qmbyjpserame.png')) }}" alt="{{$part->partsku }}" class='img-thumbnail' height='200px' width='200px' id="photoimg">
				</label>
				<input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
			</div>
		</div>
		<h6><small><em>Last Modified by: </em>{{ $currentStock->lasttouch->name or "Somebody that I used to know" }}</small></h6>
		<hr>
		<div class="panel panel-primary">
			<div class="panel-heading">
				General Information
			</div>
			
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<div class="checkbox disabled">
								<label><input type="checkbox" name="isfieldupdated[]" value="partsku" disabled> <strong>Part SKU</strong>
								</label>
							</div>
							<input name="partsku" type="text" class="form-control input-sm" value="{{ $part->partsku }}" />
						</div>
	                </div>

	                <div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschdescr" {{ ($part->haschdescr=='1' ? "checked" : "") }}> <strong>Description</strong>
								</label>
							</div>
							<input name="descr" type="text" class="form-control input-sm" value="{{ $part->descr }}" />
						</div>
	                </div>

	                <div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschwiseitemid" {{ ($part->haschwiseitemid=='1' ? "checked" : "") }}> <strong>WISE ID</strong>
								</label>
							</div>
							<input name="wiseitemid" type="text" class="form-control input-sm" value="{{ $part->wiseitemid }}" />
						</div>
	                </div>

				</div>

				<div class="row">

					<div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschparent" {{ ($part->haschparent=='1' ? "checked" : "") }}> <strong>Parent SKU</strong>
								</label>
							</div>
							<select name="parentcode" id="parentcode" class="input-sm form-control selectpicker" data-size="8" data-live-search="true">
										@foreach($parents as $parent)
                                            <option value="{{$parent->parentcode}}" {{ ($part->parentcode == $parent->parentcode ? "selected" : "") }}  data-suppcode="{{$parent->suppcode}}">{{$parent->parentcode}}</option>
                                        @endforeach;
									</select>
						</div>
					</div>
					
		
					<div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="hascholdsku" {{ ($part->hascholdsku=='1' ? "checked" : "") }}> <strong>Old SKU</strong>
								</label>
							</div>
							<input name="oldsku" type="text" class="form-control input-sm" value="{{ $part->oldsku }}" />
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschrelatedsku" {{ ($part->haschrelatedsku=='1' ? "checked" : "") }}> <strong>Related SKUs</strong>
								</label>
							</div>
							<input name="relatedsku" type="text" class="form-control input-sm" value="{{ $part->relatedsku }}" />
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-lg-4">
                        <div class="form-group">
                            <div class="checkbox disabled">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschsuppcode" disabled> <strong>Supplier
								<span class="text-success"><small><em>
                            		filled in automatically
                            	</em></small></span></strong>
								</label>
							</div>
							<select name="suppcode" id="suppcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true">
	                            @foreach($suppliers as $supp)
	                            	<option value="{{$supp->suppcode}}" {{ ($part->suppcode == $supp->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
	                            @endforeach
                            </select>
                        </div>
                    </div>

					<div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschwhloc" {{ ($part->haschwhloc=='1' ? "checked" : "") }}> <strong>Warehouse Location</strong>
								</label>
							</div>
							<input name="whloc" type="text" class="form-control input-sm" value="{{ $part->whloc }}" />
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschbulkloc" {{ ($part->haschbulkloc=='1' ? "checked" : "") }}> <strong>Bulk Location</strong>
								</label>
							</div>
							<input name="bulkloc" type="text" class="form-control input-sm" value="{{ $part->bulkloc }}" />
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
	                        <div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschqtyinbulkloc" {{ ($part->haschqtyinbulkloc=='1' ? "checked" : "") }}> <strong>Qty in Bulk Location</strong>
								</label>
							</div>
	                        <input name="qtyinbulkloc" type="number" class="form-control input-sm" value="{{ $part->qtyinbulkloc }}" />
	                    </div>
	                </div>

					<div class="col-md-4">
						<div class="form-group">
	                        <div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschbarcode" {{ ($part->haschbarcode=='1' ? "checked" : "") }}> <strong>Barcode</strong>
								</label>
							</div>
	                        <input name="barcode" type="text" class="form-control input-sm" value="{{ $part->barcode }}" />
	                    </div>
	                </div>

	                <div class="col-lg-4">
		                <div class="form-group">
		                    <div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschstatus" {{ ($part->haschstatus=='1' ? "checked" : "") }}> <strong>Status<span class="text-success"><small><em>
		                            part's current status
		                        </em></small></span></strong>
								</label>
							</div>
		                    <select name="sys_status" class="form-control input-sm" >
		                        <option value="new on order" {{ strcasecmp($currentStock->sys_status, "new on order")==0 ? "selected='true'" : ""  }}>New on Order</option>
		                        <option value="New On Order in System" {{ strcasecmp($currentStock->sys_status,"New On Order in System")==0 ? "selected='true'" : ""  }}>New on Order in System</option>
		                        <option value="Updated" {{ strcasecmp($currentStock->sys_status,"Updated") == 0 ? "selected='true'" : ""  }}>Updated</option>
		                        <option value="Pending" {{ strcasecmp($currentStock->sys_status,"Pending") == 0 ? "selected='true'" : ""  }}>Pending</option>
		                        <option value="Complete" {{ strcasecmp($currentStock->sys_status,"Complete") == 0 ? "selected='true'" : ""  }}>Complete</option>
		                        <option value="Inactive" {{ strcasecmp($currentStock->sys_status,"Inactive") == 0 ? "selected='true'" : ""  }}>Inactive</option>
		                    </select>
		                </div>
		            </div>
				</div>
				
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
	                        <div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschnotes" {{ ($part->haschnotes=='1' ? "checked" : "") }}> <strong>Notes</strong>
								</label>
							</div>
	                        <input name="notes" type="text" class="form-control input-sm" value="{{ $part->notes }}" />
	                    </div>
	                </div>

	                <div class="col-lg-4">
		                <div class="form-group">
		                    <div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschisactive" {{ ($part->haschisactive=='1' ? "checked" : "") }}> <strong>Is Active<span class="text-success"><small><em>
		                            Neto Status
		                        </em></small></span></strong>
								</label>
							</div>
		                    <select name="sys_isactive" class="form-control input-sm" >
		                        <option value="1" {{ $part->sys_isactive=='1' ? "selected='true'" : ""  }}>Yes</option>
		                        <option value="0" {{ $part->sys_isactive=='0' ? "selected='true'" : ""  }}>No</option>
		                    </select>
		                </div>
		            </div>
				</div>
				
			</div> <!-- ./Panel-body -->
				
		</div> <!-- ./Panel -->


		<!--========================-->
		<!-- DIMENSIONS				 -->
		<!--========================-->
		<div class="panel panel-primary">
			<div class="panel-heading">
				Shipping Details
			</div>
			
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschpartlen" {{ ($part->haschpartlen=='1' ? "checked" : "") }}> <strong>Length, m</strong>
								</label>
							</div>
							<input name="partlen" type="number" step="0.00001" class="form-control input-sm" value="{{$part->partlen}}" />
						</div>
	                </div>

	                <div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschpartwd" {{ ($part->haschpartwd=='1' ? "checked" : "") }}> <strong>Width, m</strong>
								</label>
							</div>
							<input name="partwidth" type="number" step="0.00001" class="form-control input-sm" value="{{$part->partwidth}}" />
						</div>
	                </div>

	                <div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschpartht" {{ ($part->haschpartht=='1' ? "checked" : "") }}> <strong>Height, m</strong>
								</label>
							</div>
							<input name="partht" type="number" step="0.00001" class="form-control input-sm" value="{{$part->partht}}" />
						</div>
	                </div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschpartwt" {{ ($part->haschpartwt=='1' ? "checked" : "") }}> <strong>Weight, kg</strong>
								</label>
							</div>
							<input name="partwt" type="number" step="0.00001" class="form-control input-sm" value="{{$part->partwt}}" />
						</div>
	                </div>

	                <div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschshipmethod" {{ ($part->haschshipmethod=='1' ? "checked" : "") }}> <strong>Shipping Method</strong>
								</label>
							</div>
							<input name="shipmethod" type="text" class="form-control input-sm" value="{{$part->shipmethod}}" >
						</div>
	                </div>

	                <div class="col-md-4">
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="isfieldupdated[]" value="haschpartprice" {{ ($part->haschpartprice=='1' ? "checked" : "") }}> <strong>Part Price</strong>
								</label>
							</div>
							<input name="price" type="number" step="any" class="form-control input-sm" value="{{$part->price}}" >
						</div>
	                </div>

				</div>
				
			</div> <!-- ./Panel-body -->
				
		</div> <!-- ./Panel -->

		<!--========================-->
		<!-- PANEL FOR STOCKS -->
		<!--========================-->
		<div class="panel panel-primary">
			<div class="panel-heading">
				Stock Control - <em>Current Stocks</em>
			</div>
			
			<div class="panel-body">
				
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<div class="checkbox">
									<label><input type="checkbox" name="isfieldupdated[]" value="haschsoh" {{ ($part->haschsoh=='1' ? "checked" : "") }}> <strong>Stocks on Hand</strong>
									</label>
								</div>
								<input name="soh" type="number" class="form-control input-sm" value="{{$currentStock->soh}}" />
							</div>
	                    </div>

	                    <div class="col-md-4">
							<div class="form-group">
								<div class="checkbox">
									<label><input type="checkbox" name="isfieldupdated[]" value="haschrestockqty" {{ ($part->haschrestockqty=='1' ? "checked" : "") }}> <strong>Re-Stock Qty</strong>
									</label>
								</div>
								<input name="restockqty" type="number" class="form-control input-sm" value="{{$currentStock->restockqty}}" />
							</div>
	                    </div>

	                    <div class="col-md-4">
							<div class="form-group">
								<div class="checkbox">
									<label><input type="checkbox" name="isfieldupdated[]" value="haschreorderqty" {{ ($part->haschreorderqty=='1' ? "checked" : "") }}> <strong>Re-Order Qty</strong>
									</label>
								</div>
								<input name="onorderqty" type="number" class="form-control input-sm" value="{{$currentStock->onorderqty}}" />
							</div>
	                    </div>
					</div>
				
				
					<div class="row">
						

						<div class="col-md-4">
							<div class="form-group">
								<div class="checkbox">
									<label><input type="checkbox" name="isfieldupdated[]" value="haschsaleslast150d" {{ ($part->haschsaleslast150d=='1' ? "checked" : "") }}> <strong>Sales last 150 days</strong>
									</label>
								</div>
								<input name="salespast150d" type="number" class="form-control input-sm" value="{{$currentStock->salespast150d}}" />
							</div>
						</div>

						<div class="col-lg-4">
                            <div class="form-group">
                                <div class="checkbox">
									<label><input type="checkbox" name="isfieldupdated[]" value="haschorderstatus" {{ ($part->haschorderstatus=='1' ? "checked" : "") }}> <strong>Order Status<span class="text-success"><small><em>
                                        Part's current order status
                                    </em></small></span></strong>
									</label>
								</div>
                                <select name="sys_finalstatus" class="form-control input-sm" >
                                	@if($currentStock->sys_finalstatus == "Unresolved" or $currentStock->sys_finalstatus == "Cancelled")
                                    	<option value="Pending" {{ $currentStock->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                    	<option value="Cancelled" {{ strcasecmp($currentStock->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                    	<option value="Unresolved" {{ (strcasecmp($currentStock->sys_finalstatus,"Unresolved") == 0) || empty($currentStock->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                    @elseif($currentStock->sys_finalstatus == "Pending")
                                    	<option value="Pending" {{ $currentStock->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                    	<option value="Cancelled" {{ strcasecmp($currentStock->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                    	<option value="Unresolved" {{ (strcasecmp($currentStock->sys_finalstatus,"Unresolved") == 0) || empty($currentStock->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                    @else
                                    	<option value="Pending" {{ $currentStock->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                    	<option value="Pending with Purchasing" {{ strcasecmp($currentStock->sys_finalstatus,"Pending with Purchasing")==0 ? "selected='true'" : ""  }}>Pending with Purchasing</option>
	                                    <option value="Pending with Daisy" {{ strcasecmp($currentStock->sys_finalstatus,"Pending with Daisy") == 0 ? "selected='true'" : ""  }}>Pending with Daisy</option>
	                                    <option value="Confirmed with Purchasing" {{ strcasecmp($currentStock->sys_finalstatus,"Confirmed with Purchasing") == 0 ? "selected='true'" : ""  }}>Confirmed with Purchasing</option>
	                                    <option value="Confirmed with Daisy" {{ strcasecmp($currentStock->sys_finalstatus,"Confirmed with Daisy") == 0 ? "selected='true'" : ""  }}>Confirmed with Daisy</option>
	                                    <option value="In Transit" {{ strcasecmp($currentStock->sys_finalstatus,"In Transit") == 0 ? "selected='true'" : ""  }}>In Transit</option>
	                                    <option value="Received" {{ strcasecmp($currentStock->sys_finalstatus,"Received") == 0 ? "selected='true'" : ""  }}>Received</option>
	                                    <option value="Cancelled" {{ strcasecmp($currentStock->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
	                                    <option value="Unresolved" {{ (strcasecmp($currentStock->sys_finalstatus,"Unresolved") == 0) || empty($currentStock->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                    @endif



                                    

                                </select>
                            </div>
                        </div>

					</div>
				
			</div> <!-- ./Panel-body -->
		</div> <!-- ./Panel -->



		<!--========================-->
		<!-- PANEL FOR ORDERS -->
		<!--========================-->
		<div class="panel panel-danger">
			<div class="panel-heading">
				Current Orders
			</div>
			
			<div class="panel-body">
				
				<table class="table table-striped">
					<thead>
						<th>Currently In</th>
						<th>Date Created</th>
						<th>Supplier</th>
						<th>Reorder Qty</th>
						<th>Total Order Qty</th>
						<th>Order Status</th>
					</thead>

					<tbody>
						@foreach($partsOrdered as $po)
							<tr>
								<td>{{ ($po->sys_finalstatus == "Pending" ? "Supplier Orders" : "Existing Orders") }}</td>
								<td>{{ date('d/m/Y',strtotime($po->created_at)) }}</td>
								<td>{{ $po->suppcode }}</td>
								<td>{{ $po->reorderqty }}</td>
								<td>{{ $po->sys_total }}</td>
								<td>{{ $po->sys_finalstatus }}</td>
							</tr>
						@endforeach
						
					</tbody>
				</table>
				
			</div> <!-- ./Panel-body -->
		</div> <!-- ./Panel -->


		<div>
			<div class="row">
				<div class="col-md-4 pull-right">
					
		        	<button class="btn btn-primary btn-block" type="submit">Save</button>
		        </div>
				<div class="col-md-4 pull-right">
					<a class="btn btn-default btn-block" href="{{url('/parts')}}">Cancel</a>
				</div>
				
			</div>
		</div>
	</form>
@stop

@section('userdefjs')
	<script>
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $('#photoimg').attr('src', e.target.result);
		        }
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$(function(){
			$("#photo").on('change', function() {
				//$("#photoimg").prop("src", $("#photo").val().substring(12));				
				readURL(this);
			});

			$("#partsFormEdit").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');
            });

			$(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(0).addClass('active');

		})
	</script>
@stop