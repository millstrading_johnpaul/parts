@extends('layout')

@section('content')
	<div class="exportPage text-left">
		<h3 class="text-center">Export Data to CSV </h3>
		<h5 class="text-center"><small><em>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, magni. 
		<br>
			Check boxes on the left to select what to export.
		</em></small><h5>
		
		<hr>
		<h5>
			<small> 
			</small>
		</h5>
	
			<div class="row dataExportSelect">

				<div class="col-lg-4">
					<h4 class="text-primary">Data Type</h4>
				</div>
				<div class="col-lg-8">
					<h4>Select Data to Export</h4>
					<h4><small>The type of data selected determines your export.</small></h4>

					<div class="form-group">
						<div class="radio">
							<label>
								<input type="radio" name="optDataExport" value="1" checked>Parts Inventory
							</label>
						</div>

						<div class="radio">
							<label>
								<input type="radio" name="optDataExport" value="2">Back Orders
							</label>
						</div>

						<div class="radio">
							<label>
								<input type="radio" name="optDataExport" value="3">Special Orders
							</label>
						</div>

						<div class="radio">
							<label>
								<input type="radio" name="optDataExport" value="4">Open Boxes
							</label>
						</div> 

						<div class="radio">
							<label>
								<input type="radio" name="optDataExport" value="6">RMA Awaiting Parts
							</label>
						</div> 
						
						<div class="radio">
							<label>
								<input type="radio" name="optDataExport" value="5">Existing Orders
							</label>
						</div>
					</div> 

				</div><br>

			</div>

			<div class="row">

				<div class="col-lg-4">
					<h4 class="text-primary">Export Data</h4>
				</div>

				<div class="col-lg-8">
					<h4>Download</h4>
					<h4><small>Click download button to download the selected data above.</small></h4>
					<a href="{{url('/export/parts')}}" class="btn btn-success col-lg-4" id="btnIVDownload">
					<i class="fa fa-download"></i> Download File (xlsx format)</a>
				</div>
					
			
			</div>

		

		<div class="row">
			<div class="col-lg-12">
				&nbsp;
			</div>
		</div>
	</div>
@stop

@section('userdefjs')
	<script>
		$(function(){
			$(".navmenuitemlist li.dropdown").removeClass('active').eq(2).addClass('active');
        	$(".navsubmenuitemlist li").removeClass('active').eq(8).addClass('active');

			$("input[name='optDataExport']").on('change', function(event) {
				event.preventDefault();
				/* Act on the event */

				
				selectedOptRadioVal = $("input:radio[name='optDataExport']:checked").val().trim();
				switch(selectedOptRadioVal){
					case '1': $("#btnIVDownload").removeAttr('href').attr("href", "{{url('/export/parts')}}");
						break;

					case '2': $("#btnIVDownload").removeAttr('href').attr("href", "{{url('/export/backorders')}}");
						break;

					case '3': $("#btnIVDownload").removeAttr('href').attr("href", "{{url('/export/specialorders')}}");
						break;

					case '4': $("#btnIVDownload").removeAttr('href').attr("href", "{{url('/export/openboxes')}}");
						break;

					case '5': $("#btnIVDownload").removeAttr('href').attr("href", "{{url('/export/existingorders')}}");
						break;

					case '6': $("#btnIVDownload").removeAttr('href').attr("href", "{{url('/export/raps')}}");
						break;

					default:
						break;
				}
				

			});

		});
	</script>
@stop
