<html> 
    <body> <h1>Existing Orders</h1> 

    @if(!empty($existingorders)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Date Created</th>
                    <th>SP Number</th>
                    <th>Parent SKU</th>
                    <th>Supplier</th>
                    <th>Part SKU</th>
                    <th>Description</th>
                    <th>Order Ave</th>
                    <th>ETA</th>
                    <th>Qty</th>
                    <th>Order Status</th>
                </tr>
            </thead>
            <tbody>
                    @if(count($backorders)>0)
                        @foreach($backorders as $rowitem)
                            @php
                                $exords = $existingorders->where("id", $rowitem->supplierorder_id)->first(); 
                            @endphp
                            @if(!is_null($exords))
                                <tr>
                                    <td>{{$exords->created_at->format("d-m-Y")}}</td>
                                    <td>{{$exords->spnum}}</td>
                                    <td>{{$rowitem->parentcode or "-"}}</td>
                                    <td>{{$rowitem->suppcode or "-"}}</td>
                                    <td>{{$rowitem->partsku}}</td>
                                    <td>{{$rowitem->descr or "-"}}</td>
                                    <td>{{$exords->orderavenue}}</td>
                                    <td>{{$exords->eta}}</td>
                                    <td>{{ (is_null($rowitem->qty) ? (is_null($rowitem->reorderqty) ? "-" : $rowitem->reorderqty) : $rowitem->qty) }}</td>
                                    <td>{{$exords->sys_finalstatus}}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endif

                    @if(count($specialorders)>0)
                        @foreach($specialorders as $rowitem)
                            @php
                                $exords = $existingorders->where("id", $rowitem->supplierorder_id)->first(); 
                            @endphp
                            @if(!is_null($exords))
                                <tr>
                                    <td>{{$exords->created_at->format("d-m-Y")}}</td>
                                    <td>{{$exords->spnum}}</td>
                                    <td>{{$rowitem->parentcode or "-"}}</td>
                                    <td>{{$rowitem->suppcode or "-"}}</td>
                                    <td>{{$rowitem->partsku}}</td>
                                    <td>{{$rowitem->descr or "-"}}</td>
                                    <td>{{$exords->orderavenue}}</td>
                                    <td>{{$exords->eta}}</td>
                                    <td>{{ (is_null($rowitem->qty) ? (is_null($rowitem->reorderqty) ? "-" : $rowitem->reorderqty) : $rowitem->qty) }}</td>
                                    <td>{{$exords->sys_finalstatus}}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endif

                    @if(count($openboxes)>0)
                        @foreach($openboxes as $rowitem)
                            @php
                                $exords = $existingorders->where("id", $rowitem->supplierorder_id)->first(); 
                            @endphp
                            @if(!is_null($exords))
                                <tr>
                                    <td>{{$exords->created_at->format("d-m-Y")}}</td>
                                    <td>{{$exords->spnum}}</td>
                                    <td>{{$rowitem->parentcode or "-"}}</td>
                                    <td>{{$rowitem->suppcode or "-"}}</td>
                                    <td>{{$rowitem->partsku}}</td>
                                    <td>{{$rowitem->descr or "-"}}</td>
                                    <td>{{$exords->orderavenue}}</td>
                                    <td>{{$exords->eta}}</td>
                                    <td>{{ (is_null($rowitem->qty) ? (is_null($rowitem->reorderqty) ? "-" : $rowitem->reorderqty) : $rowitem->qty) }}</td>
                                    <td>{{$exords->sys_finalstatus}}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endif

                    @if(count($raps)>0)
                        @foreach($raps as $rowitem)
                            @php
                                $exords = $existingorders->where("id", $rowitem->supplierorder_id)->first(); 
                            @endphp
                            @if(!is_null($exords))
                                <tr>
                                    <td>{{$exords->created_at->format("d-m-Y")}}</td>
                                    <td>{{$exords->spnum}}</td>
                                    <td>{{$rowitem->parentcode or "-"}}</td>
                                    <td>{{$rowitem->suppcode or "-"}}</td>
                                    <td>{{$rowitem->partsku}}</td>
                                    <td>{{$rowitem->descr or "-"}}</td>
                                    <td>{{$exords->orderavenue}}</td>
                                    <td>{{$exords->eta}}</td>
                                    <td>{{ (is_null($rowitem->qty) ? (is_null($rowitem->reorderqty) ? "-" : $rowitem->reorderqty) : $rowitem->qty) }}</td>
                                    <td>{{$exords->sys_finalstatus}}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endif

                    @if(count($stocks)>0)
                        @foreach($stocks as $rowitem)
                            @php
                                $exords = $existingorders->where("id", $rowitem->supplierorder_id)->first(); 
                            @endphp
                            @if(!is_null($exords))
                                <tr>
                                    <td>{{$exords->created_at->format("d-m-Y")}}</td>
                                    <td>{{$exords->spnum}}</td>
                                    <td>{{$rowitem->parentcode or "-"}}</td>
                                    <td>{{$rowitem->suppcode or "-"}}</td>
                                    <td>{{$rowitem->partsku}}</td>
                                    <td>{{$rowitem->descr or "-"}}</td>
                                    <td>{{$exords->orderavenue}}</td>
                                    <td>{{$exords->eta}}</td>
                                    <td>{{ (is_null($rowitem->qty) ? (is_null($rowitem->reorderqty) ? "-" : $rowitem->reorderqty) : $rowitem->qty) }}</td>
                                    <td>{{$exords->sys_finalstatus}}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endif

                    
            </tbody>
        </table> 

    @endif 
    
    
    </body> 


</html>
