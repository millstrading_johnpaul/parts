<html> 
    <body> <h1>Existing Orders</h1> 

    @if(!empty($exords)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Date Created</th>
                    <th>SP Number</th>
                    <th>Parent SKU</th>
                    <th>Supplier</th>
                    
                    <th>Part SKU</th>
                    <th>Description</th>
                    <th>Order Ave</th>
                    <th>ETA</th>
                    <th>Notes</th>

                    <th>Reorderqty Qty</th>
                    <th>Backorder Qty</th>
                    <th>Openbox Qty</th>
                    <th>Specialorder Qty</th>
                    <th>RAP Qty</th>
                    
                    <th>Total</th>
                    <th>Order Status</th>
                </tr>
            </thead>
            <tbody>
                    @if(count($exords)>0)
                    
                        @foreach($exords as $rowitem)
                            <tr>
                                <td>{{$rowitem->created_at->format("d-m-Y")}}</td>
                                <td>{{$rowitem->spnum}}</td>
                                <td>{{$rowitem->parentcode}}</td>
                                <td>{{$rowitem->suppcode}}</td>

                                <td>{{$rowitem->partsku}}</td>
                                <td>{{$rowitem->descr}}</td>
                                <td>{{$rowitem->orderavenue}}</td>
                                <td>{{$rowitem->eta}}</td>
                                <td>{{$rowitem->notes}}</td>
                                
                                <td>{{$rowitem->reorderqty or "-"}}</td>
                                <td>{{$rowitem->backorder or "-"}}</td>
                                <td>{{$rowitem->openbox or "-"}}</td>
                                <td>{{$rowitem->specialorder or "-"}}</td>
                                <td>{{$rowitem->rap or "-"}}</td>
                                
                                <td>{{$rowitem->sys_total}}</td>
                                <td>{{$rowitem->sys_finalstatus}}</td>
                            </tr>
                        @endforeach
                    @endif

                    
            </tbody>
        </table> 

    @endif 
    
    
    </body> 


</html>
