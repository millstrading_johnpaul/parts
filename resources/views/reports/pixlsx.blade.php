<html> 
    <body> <h1>Part SKUs</h1> 

    @if(!empty($partskus)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Part SKU</th>
                    <th>Description</th>
                    <th>Supplier</th>
                    <th>Parent SKU</th>
                    <th>Old SKU</th>
                    <th>Related SKU</th>
                    <th>WH Location</th>
                    <th>Bulk Location</th>
                    <th>Barcode</th>

                    <th>WISE Item Id</th>
                    <th>SOH</th>
                    <th>Re-Stock Qty</th>
                    <th>Re-Order Qty</th>
                    <th>SalesPast150d</th>
                    <th>Part Status</th>
                    <th>Order Status</th>
                    
                </tr>
            </thead>
            
            @foreach ($partskus as $row)
                    
                <tr>
                    <td>{{$row->partsku}}</td>
                    <td>{{$row->descr}}</td>
                    <td>{{$row->suppcode}}</td>
                    <td>{{$row->parentcode}}</td>
                    <td>{{$row->oldsku}}</td>

                    <td>{{$row->relatedsku}}</td>
                    <td>{{$row->whloc}}</td>
                    <td>{{$row->bulkloc}}</td>
                    <td>{{$row->barcode}}</td>
                    <td>{{$row->wiseitemid}}</td>
                    <td>{{$row->soh}}</td>
                    <td>{{$row->restockqty}}</td>
                    <td>{{$row->onorderqty}}</td>
                    <td>{{$row->salespast150d}}</td>
                    <td>{{$row->sys_status}}</td>
                    <td>{{$row->sys_finalstatus}}</td>
                    
                </tr>
                   
            @endforeach 
        </table> 

    @endif 
    </body> 
</html>