@extends('layout')

@section('content')
	
		<h2 class="thin"><span style="color:blue;"><strong>Archive</strong> RAPs</span></h3>
		<p class="muted">List of RAPs that is available for archiving. Check items that you want to archive, then click Archive button.</p>
		
		<ol class="breadcrumb text-left">
			<li class="active">RMA Awaiting Parts</li>
			<li><a href="{{url('/settings/archive/unarchive/rap')}}">Un-Archive</a></li>
		</ol>

		<hr>

		@if(Session::has('there-has-been-an-archived-rap-message'))
            <div class="alert alert-success text-left">
                {{ Session::get('there-has-been-an-archived-rap-message') }}
            </div>
        @endif

		<div class="text-left">
			<div id="loaderrap" class="text-center">
				<img src="{{url('/images/app/rolling.gif')}}">
			</div>
			<table class="table table-hover" id="raptable" cellspacing="0" width="100%">
				<thead>
					<th >Date Created</th>
                    <th >RMA Number</th>
                    <th >Location</th>
                    <th >Parent SKU</th>
                    <th >Supplier</th>
                    <th >Part SKU</th>
                    <th >Kayako ID</th>
                    <th >Qty</th>
                    <th >RAP Status</th>
                    <th >Order Status</th>
                    <th>Archive?</th>
				</thead>
			
				<tbody class="hidden">
					
				</tbody>

				<tfoot>
					<th colspan="11" rowspan="1" class="text-right" id="buttoncontainer">
					</th>
				</tfoot>
			</table>


		</div>

		<form id="form_archiverap" action=" {{ url('/settings/archive/archiveselectedrap') }}" method="POST" enctype="multipart/form-data">
			<input type="text" class="hidden" id="selected_archiverap" name="selected_archiverap">
			{{ csrf_field() }}
		</form>

		

	</div>
@stop


@section('userdefjs')
	<script>
		function formatThisDate(thisDate){

			return (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   )  + "/"  +
				(  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "/" + 
				thisDate.getFullYear();
		}

		$(document).ready(function() {
			$(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(4).addClass('active');
			
			
			var table = $('#raptable')
				.on( 'init.dt', function () {
			        $("#raptable tbody").removeClass('hidden');
			        $("#loaderrap").addClass('hidden');
			    })
				.DataTable({
				 	"processing": true,
					"ajax": '{{ url('/json/forarchiverap.json') }}',
				"columns": [
					{
		            	data: 'created_at',
		            	"render": function ( data, type, full, meta ) {
		            		var d = new Date(data);

							return formatThisDate(d);
					    }
		        	},
		            { "data": "rmanum" }, //4
		            { "data": "whloc" }, //5
		            { "data": "parentcode" }, //6
		            { "data": "suppcode" }, //7
		            { "data": "partsku" }, //8
		            { "data": "kayakoid" }, //12
		            { "data": "qty" }, //9
		            { "data": "sys_orderstatus" }, //10
		            { "data": "sys_finalstatus" }, //11
		            { 
			            	"className": 'select-checkbox',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ''
			            },
		            ],
		       
		        	"select": {
			        	style: 'multi',
			        	selector: 'td:last-child'
			        },
			        dom: 'Bfrtip',
		       		"buttons": [
				            { 
					            extend: 'selected',
					            text: '<i class="fa fa-sign-in"></i> Archive selected',
					            className: 'btn-success',
					            action: function ( e, dt, button, config ) {
					                //alert( dt.rows( { selected: true } ).indexes().length +' row(s) selected' );
					                $("#selected_archiverap").val(JSON.stringify(dt.rows({selected: true}).data().toArray()));
				                	$("#form_archiverap").submit();
					            }
					        }	
			        ],
		        	'deferRender': 'true',
		            "order": [[1, 'asc']]
			});


			table.buttons().container().appendTo( $('#buttoncontainer') );


			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');
			$("th.clone-control").removeClass('sorting_asc').addClass('sorting_disabled');



		}); //End Document Ready

		
	</script>
@stop