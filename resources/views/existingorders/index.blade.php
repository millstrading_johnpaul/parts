@extends('layout')

@section('content')
	
		<h2 class="thin">Existing Orders</h3>
		<p class="muted">We wait for these to arrive. If it doesn't, we still wait. There is no other option but to wait. </p>
		
		<hr>
	
		<div>
			<div id="loaderex" class="text-center">
				<img src="{{url('/images/app/rolling.gif')}}">
			</div>
			<table class="table table-striped" id="extable" cellspacing="0" width="100%">
				<caption><a class="btn btn-default" href="{{url('/export/existingorders/persku')}}">Download per Sku</a></caption>
				<thead>
					<th width="3%"></th>
					<th width="4%"></th>
					<th >Date Created</th>
                    <th >Supplier</th>
                    <th >Parent</th>
                    <th >Part SKU</th>
                    <th class="text-center">Total</th>
                    <th>Order Status</th>
                    <th>PO/SP Num</th>
                    <th>ETA</th>
                    <th class="hidden">Id</th>
                    <th class="hidden">Description</th>
				</thead>

				<tbody class="text-left hidden">					
				</tbody>
			</table>
	
	
		</div>
	
	
	
	
@stop

@section('userdefjs')
	<script>
		function formatThisDate(thisDate){
			/*return thisDate.getFullYear() + "-" + (  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "-" + (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   );*/

			return (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   )  + "/"  +
				(  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "/" + 
				thisDate.getFullYear();
		}
		function getKabits(id){
			mydata = "";

			$.ajax({
				url: '/parts/public/existingorders/getSubsequentDetails/' + id,
				type: 'GET',
				success: function(data){
					$(".fakedTb").append(data);

				}
			});
			

			return mydata;
		}

		function format(d){
			html = "" + '<i><div class="col-lg-10 col-lg-offset-1">' + '<table class="table table-danger">' +
				'<caption><a href="{{ url('/export/supplierorders/') }}/'+d.id+'" class="pull-right">Download Export</a></caption>' +
				'<thead><tr class="info">'+
		            '<td width="20%">SKU</td>'+
		            '<td width="30%">Description</td>'+
		            '<td width="10%">ReO</td>'+
		            '<td width="10%">OBP</td>'+
		            '<td width="10%">RAP</td>'+
		            '<td width="10%">BkO</td>'+
		            '<td width="10%">SpO</td>'+
		        '</tr></thead>'+ 
		        '<tbody class="fakedTb">';

		    html += '</tbody></table></div></i>';

			return html;
		}

		$(function(){
			$(".navmenuitemlist li.dropdown").removeClass('active').eq(1).addClass('active');
        	$(".navsubmenuitemlist li").removeClass('active').eq(6).addClass('active');

			var table = $('#extable')
				.on( 'init.dt', function () {
			        $("#extable tbody").removeClass('hidden');
			        $("#loaderex").addClass('hidden');
			    })
				.DataTable({
					"ajax": '{{url('/json/existingordersjson.json')}}',
					"columns": [
				            {
				                "className":'details-control',
				                "orderable":      false,
				                "data":           null,
				                "defaultContent": ''
				            }, //0
				            {
				                "className":'edit-control',
				                "orderable":      false,
				                "searchable": false,
				                "data": null,
				                "render": function ( data, type, full, meta ) {
							      	return "<td style='text-align: center; vertical-align: middle;'><a href='/parts/public/existingorders/"+data.id+"/edit' data-toggle='tooltip' data-placement='top' title='Click to modify.'><img src='/parts/public/images/app/details_edit.png'></a></td>";
							    }
				            },//1
				            { "data": "created_at" },//2
				            { "data": "suppcode" },//3
				            { "data": "parentcode" },//4
				            { "data": "partsku" },//5

				            { "data": "sys_total" },//6
				            { "data": "sys_finalstatus" },//7
				            { "data": "spnum" },//8
				            { "data": "eta" },//9
				          	{ "data": "id" },//10
				            { "data": "descr" },//11
			         
			            ],
			        "columnDefs": [
			        	{
			        		"targets": [4],
			        		render: function ( data, type, row ) {
							     return type === 'display' && data.length > 20 ?
							        data.substr( 0, 25 ) +'…' :
							        data;
							}
			        	},
			        	
			            {
			                "targets": [ 10, 11 ],
			                "visible": false,
			                "searchable": false
			            },
			            {
			            	"targets": [2, 9],
			            	"render": function ( data, type, full, meta ) {
			            		var d = new Date(data);
			            		if (isNaN(d)) {
			            			return "--/--/----";
			            		}
								return formatThisDate(d);
						    }
			            },
			        ],
			        "dom": 'Bfrtip',
			        "buttons": [
				        {
				            extend: 'csv',
				            text: '<i class="fa fa-download"></i> Download CSV',
				            className: 'btn-primary',
				            title: 'ExistingOrders_Export'
				        }
				    ],
			       		select: 'single',
			       
			            "order": [[1, 'asc']]
			});

			//$("#extable_filter").closest("div").addClass('text-right');
			$("#extable_filter").addClass('pull-right');
			$("#extable_wrapper > div.dt-buttons").css("display", "block");


			$('#extable tbody').on('click', 'td.details-control', function () {
			    var tr = $(this).closest('tr');
			    var row = table.row( tr );
			 
			    if ( row.child.isShown() ) {
			        // This row is already open - close it
			        row.child.hide();
			        tr.removeClass('shown');
			    }
			    else {
			        if ( table.row( '.shown' ).length ) {
						$('.details-control', table.row( '.shown' ).node()).click();
					}

					row.child( format(row.data()) ).show();
					tr.addClass('shown');
					getKabits(row.data().id);
			    }
			});

			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');
		});
	</script>
@stop
