@extends('layout')


@section('content')
	<!--========================-->
	<!-- BASIC INFORMATION -->
	<!--========================-->
	
	<h2 class="thin">Modify Existing Order</h2>
	<p class="muted">Yeah. Text here.</p>
	
	<ol class="breadcrumb text-left">
		<li><a href="/parts/public/existingorders">Existing Orders</a></li>
		<li class="active">Edit</li>
	</ol>
	
	<hr>
	<form method="POST" action="{{ url('/existingorders/modify/'. $exOrd->id )}}" class="text-left" enctype="multipart/form-data">
		{{ method_field('PATCH') }}
		{{ csrf_field() }}
		<div class="row">
			<div class="col-lg-6">
				<h3>{{ $exOrd->suppcode }}</h3>
				<h5><small><em>supplier</em></small></h5>			
			</div>

			<div class="col-lg-6 text-right">
				<h3 class="text-danger">{{  $exOrd->created_at->format("d-m-Y")  }}</h3>
				<h5><small><em>date created</em></small></h5>		
			</div>
		</div>
		<h6><small><em>Last Modified by: </em>{{ $exOrd->lasttouch->name or "Somebody that I used to know" }}</small>
		<hr>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
				<div id="buttoncontainer" class="pull-right">
					
				</div>
				<a href="{{url('/existingorders/addso/'.$exOrd->id)}}" class="btn btn-success">Additional Order</a> 

				<br><br>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
				

				<div class="panel panel-primary">
						<div class="panel-heading">List of Orders 
							<span class="text-success pull-right">
								<em><strong>
									{{ $exOrd->sys_total }}
								</strong>
							 in total.</em>
							</span></div>

						<!-- Table -->
						<table class="table table-striped" id="suppoedittable">
							<thead>
								<th>Type</th>
								<th>PartSKU</th>
								<th>Description</th>
								<th>Qty</th>
								<th class="hidden">ID</th>
								<th>Status Change</th>
							</thead>
							<tbody>

							@foreach($consolidated as $item)
									<tr>

								@if(strpos(get_class($item), "Specialorder"))	
									<td>Special Order</td>
									<td>
										<i><a href='{{url('/specialorders/'.$item->id.'/edit')}}'>{{ $item->partsku }}</a></i>
									</td>
								@elseif(strpos(get_class($item), "Backorder"))
									<td>Back Order</td>
									<td>
										<i><a href='{{url('/backorders/'.$item->id.'/edit')}}'>{{ $item->partsku }}</a></i>
									</td>
								@elseif(strpos(get_class($item), "Openbox"))
									<td>Openbox</td>
									<td>
										<i><a href='{{url('/openboxes/'.$item->id.'/edit')}}'>{{ $item->partsku }}</a></i>
									</td>
								@elseif(strpos(get_class($item), "Rap"))
									<td>Rap</td>
									<td>
										<i><a href='{{url('/raps/'.$item->id.'/edit')}}'>{{ $item->partsku }}</a></i>
									</td>
								@elseif(strpos(get_class($item), "Stock"))
										<td>Needs Ordering</td>
										<td>
											<i><a href='{{url('/partspms/'.$item->partsku_id.'/edit')}}'>{{ $item->partsku }}</a></i>
										</td>
								@endif

									<td>{{ $item->descr or $item->partskuIbelong->descr }}</td>
									<td>{{ $item->qty or $item->reorderqty }}</td>
									<td class="hidden">{{ $item->id }}</td>
									<td></td>
											
								</tr>
								
							@endforeach

							</tbody>
						</table>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Order Information
					</div>
					
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="">PO/SP Number: </label>
							      	<input type="text" name="spnum" class="form-control input-sm" value="{{ $exOrd->spnum }}" autofocus="" />
								</div>
			                </div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="">Order Avenue: </label>
							      	<select name="orderavenue" class="form-control input-sm">
										<option value="Container" {{ ($exOrd->orderavenue == "Container" ? "selected" : "") }}>Container</option>
										<option value="DHL" {{ ($exOrd->orderavenue == "DHL" ? "selected" : "") }}>DHL</option>
										<option value="China Post" {{ ($exOrd->orderavenue == "China Post" ? "selected" : "") }}>China Post</option>
										<!-- <option value="Web" {{-- ($exOrd->orderavenue == "Web" ? "selected" : "") --}}>Web</option> -->
									</select>
								</div>
			                </div>

			                <div class="col-md-4 pull-right">
								<div class="form-group">
									<label for="">Parts Ordered</label>
									<input type="number" name="sys_total" class="form-control input-sm text-right" value="{{ $exOrd->sys_total }}" readonly="" />
								</div>
							</div>
						
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="">Status: </label>
							      	<select name="sys_finalstatus" class="form-control input-sm" >
		                                    <option value="Pending with Purchasing" {{ strcasecmp($exOrd->sys_finalstatus,"Pending with Purchasing")==0 ? "selected='true'" : ""  }}>Pending with Purchasing</option>
		                                    <option value="Pending with Daisy" {{ strcasecmp($exOrd->sys_finalstatus,"Pending with Daisy") == 0 ? "selected='true'" : ""  }}>Pending with Daisy</option>
		                                    <option value="Confirmed with Purchasing" {{ strcasecmp($exOrd->sys_finalstatus,"Confirmed with Purchasing") == 0 ? "selected='true'" : ""  }}>Confirmed with Purchasing</option>
		                                    <option value="Confirmed with Daisy" {{ strcasecmp($exOrd->sys_finalstatus,"Confirmed with Daisy") == 0 ? "selected='true'" : ""  }}>Confirmed with Daisy</option>

		                                    <option value="In Transit" {{ strcasecmp($exOrd->sys_finalstatus,"In Transit") == 0 ? "selected='true'" : ""  }}>In Transit</option>
		                                    <option value="Received" {{ strcasecmp($exOrd->sys_finalstatus,"Received") == 0 ? "selected='true'" : ""  }}>Received</option>
		                                    <option value="Cancelled" {{ strcasecmp($exOrd->sys_finalstatus,"Cancelled") == 0 ? "
		                                    selected='true'" : ""  }}>Cancelled</option>
		                                     <option value="Unresolved" {{ (strcasecmp($exOrd->sys_finalstatus,"Unresolved") == 0) || empty($exOrd->sys_finalstatus) ? "
		                                    selected='true'" : ""  }}>Unresolved</option>

		                                </select>
								</div>
			                </div>

			                <div class="col-md-4 pull-right">
								<div class="form-group">
									<label for="">ETA</label>
									<input type="date" name="eta" class="form-control input-sm" value="{{$exOrd->eta or 'dd/mm/yyyy'}}" />
								</div>
							</div>


						
						</div>

						<div class="row">
			                <div class="col-md-8">
								<div class="form-group">
									<label for="">Notes</label>
									<input type="text" class="form-control input-sm" name="notes" value="{{$exOrd->notes}}" />
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<div class="checkbox">
									    <label><br>
									      	<input class="packlistreceived" type="checkbox" name="packlistreceived" {{ ($exOrd->packlistreceived=='1' ? "checked='checked'" : "") }}> Packing List Received
									    </label>
								  	</div>
								</div>
							</div>
						
						</div>

						
						
						

						
					</div> <!-- ./Panel-body -->
						
				</div> <!-- ./Panel -->
			</div>
		</div>	

		<div>
			<div class="row">
				<div class="col-md-4 pull-right">
					
		        	<button class="btn btn-primary btn-block" type="submit">Save</button>
		        </div>
				<div class="col-md-4 pull-right">
					<a class="btn btn-default btn-block" href="{{url('/existingorders')}}">Cancel</a>
				</div>
				
			</div>
		</div>
	</form>

	<form id="formToPending" method="POST" action="{{url('/existingorders/'.$exOrd->id.'/pendingize')}}" class="text-left" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input id="to_pending" name="to_pending" type="text" class="form-control hidden">
	</form>
@stop

@section('userdefjs')
	<script>
		$(function(){

			var table = $("#suppoedittable")
				.DataTable({
			        "columns": [
						{ "data": "type" },
						{ "data": "partsku" },
						{ "data": "descr" },
						{ "data": "qty" },
						{ "data": "id" },
						{ 
			            	"className": 'select-checkbox text-center',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ''
			            } //last

	        		],
		        "columnDefs": [
			            {
			                "targets": [ 4 ],
			                "visible": false,
			                "searchable": false
			            }

		        	],
	        	"select": {
		        	style: 'multi',
		        	selector: 'td:last-child'
		        },
		        dom: 'Bfrtip',
	       		"buttons": [
			            {
			            	extend: 'selected',
				            text: 'Back to Pending',
				            className: 'btn-danger',
				            action: function ( e, dt, button, config ) {
				                
				                $("#to_pending").val(JSON.stringify(dt.rows({selected: true}).data().toArray()));
				               	$("#formToPending").submit();
				            }
				        },
			        ],

		        "bFilter": false,
		        "bPaginate": false,
		        "bInfo": false,
		        "order": [[1, 'asc']],
		    } );

			table.buttons().container().appendTo( $('#buttoncontainer') );
			$(".navmenuitemlist li.dropdown").removeClass('active').eq(1).addClass('active');
        	$(".navsubmenuitemlist li").removeClass('active').eq(6).addClass('active');
			
		})
	</script>
@stop
