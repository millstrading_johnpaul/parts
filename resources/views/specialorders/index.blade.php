@extends('layout')

@section('content')
		<h2 class="thin">Special Orders</h3>
		<p class="muted">Orders for Non SKUd Parts</p>

		<ol class="breadcrumb text-left">
			<li class="active">Special Orders</li>
		</ol>

		<hr>

		<div class="text-left">
			<div id="loaderspecial" class="text-center">
				<img src="/parts/public/images/app/rolling.gif">
			</div>
			<table class="table table-striped" id="sptable" cellspacing="0" width="100%">
				<thead>
					<th width="3%"></th><!-- 0 -->
					<th width="4%"></th><!-- 1 -->
					<th >Date Created</th><!-- 2 -->
                    <th >Order #</th><!-- 3 -->
                    <th >Parent</th><!-- 4 -->
                    <th >Supplier</th><!-- 5 -->
                    <th >Part SKU</th><!-- 6 -->
                    <th >Quantity</th><!-- 7 -->
                    <th >SpO Status</th><!-- 8 -->
                    <th >Order Status</th><!-- 9 -->

                    <th >Description</th><!-- 10 -->
                    <th >Fault</th><!-- 11 -->
                    <th >RMA</th><!-- 12 -->
                    <th >ID</th><!-- 13 -->
                    
				</thead>
				<tfoot>
					<th colspan="14" rowspan="1"><a href="/parts/public/specialorders/create" data-toggle='tooltip' data-placement="top" title="Create Special Order"><i class="fa fa-plus"></i> New Special Order</a></th>
				</tfoot>
			
				<tbody class="hidden">
				</tbody>
			</table>


		</div>

		

	
@stop


@section('userdefjs')
	<script>
		function formatThisDate(thisDate){
			/*return thisDate.getFullYear() + "-" + (  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "-" + (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   );*/

			return (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   )  + "/"  +
				(  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "/" + 
				thisDate.getFullYear();
		}

		function getImage(obj){
			
			mydata = "";
			myModalData = "";
			$.ajax({
				url: '/parts/public/specialorders/getimage/' + obj,
				type: 'GET',
				success: function(data){
					console.log(data);
					mydata  += "<a href='' data-toggle='modal' data-target='#myModal'><img src='data:image/jpeg;base64," + data + "' alt='Photo' class='img-thumbnail' width='100' height='100'></a>";

					myModalData  += "<img src='data:image/jpeg;base64," + data + "' alt='Photo' class='img-responsive'>";
					
					$(".appendhere").append(mydata);
					$(".appendheremodal").empty().append(myModalData);

				}
			});
			

			return mydata;
		}

		var removeElements = function(text, selector) {
		    var wrapped = $("<div>" + text + "</div>");
		    
		    return wrapped.text();
		}

		function format(d){
			return '<table class="table table-default" width="40%">'+
		        '<tr class="info">'+
		            '<td width="20%"><strong>' + removeElements(d.partsku,"a") +'</strong></td>'+
		            '<td width="30%"><em>' + d.descr +'</em></td>'+
		            '<td width="20%"></td>'+
		            '<td width="30%"></td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Supplier:</td>'+
		            '<td>' + d.suppcode +'</td>'+
		            '<td>RMA:</td>'+
		            '<td>'+ d.rma +'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Fault:</td>'+
		            '<td colspan="4">'+ d.fault +'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Image Thumbnail:</td>'+
		            '<td class="appendhere"></td>'+
		        '</tr>'+
		    '</table>';
		}

		$(document).ready(function() {
			$(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(1).addClass('active');
			

			var table = $('#sptable')
				.on( 'init.dt', function () {
			        $("#sptable tbody").removeClass('hidden');
			        $("#loaderspecial").addClass('hidden');
			    })
				.DataTable({
					"ajax": '/parts/public/json/specialordersjson.json',
					"columns": [
				            {
				                "className":'details-control',
				                "orderable":      false,
				                "data":           null,
				                "defaultContent": ''
				            }, //0
				            {
				                "className":'edit-control',
				                "orderable":      false,
				                "searchable": false,
				                "data": null,
				                "render": function ( data, type, full, meta ) {
							      	return "<td style='text-align: center; vertical-align: middle;'><a href='/parts/public/specialorders/"+data.id+"/edit' data-toggle='tooltip' data-placement='top' title='Click to modify.'><img src='/parts/public/images/app/details_edit.png'></a></td>";
							    }
				            },  //1
				            { "data": "created_at" },  //2
				            { "data": "ordernum" },  //3
				            { "data": "parentcode" },  //4
				            { "data": "suppcode" }, //5
				            { "data": "partsku" }, //6
				            { "data": "qty" },  //7
				            { "data": "sys_orderstatus" }, //8
				            { "data": "sys_finalstatus" }, //9

				            { "data": "descr" }, //10
				            { "data": "fault" }, //11
				            { "data": "rma" }, //12
				            { "data": "id" } //13
		            	],
			        "columnDefs": [
			        		{
				                "targets": [ 2, 10, 11,12,13 ],
				                "visible": false,
				                "searchable": false
				            },
				            
				            {
				            	"targets": [2],
				            	"render": function ( data, type, full, meta ) {
				            		var d = new Date(data);

									return formatThisDate(d);
							    }
				            },

			        	],
			        "dom": 'Bfrtip',
			        "buttons": [
				        {
				            extend: 'csv',
				            text: '<i class="fa fa-download"></i> Download CSV',
				            className: 'btn-primary',
				            title: 'SpecialOrders_Export'
				        }
				    ],
		        	"deferRender": true		            
			});


			           
			        

			$('#sptable tbody').on('click', 'td.details-control', function () {
			    var tr = $(this).closest('tr');
			    var row = table.row( tr );
			 
			    if ( row.child.isShown() ) {
			        // This row is already open - close it
			        row.child.hide();
			        tr.removeClass('shown');
			    }
			    else {
			        if ( table.row( '.shown' ).length ) {
						$('.details-control', table.row( '.shown' ).node()).click();
					}

					row.child( format(row.data()) ).show();
					tr.addClass('shown');
					getImage(row.data().id);
			    }
			});


			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');
			$("#sptable_filter").addClass('pull-right');

		}); //End Document Ready

		
	</script>
@stop
