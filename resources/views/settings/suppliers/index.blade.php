@extends('layout')

@section('content')
    
        <h2 class="thin">Suppliers List</h3>
        <p class="muted">The list of all Suppliers used in the system.</p>
        
        <ol class="breadcrumb text-left">
            <li class="active">Suppliers</li>
        </ol>

        <hr>

        <div class="text-left">
            <div id="loaderopen" class="text-center">
                <img src="{{url('/images/app/rolling.gif')}}">
            </div>
            <div class="col-lg-8 col-lg-offset-2">
                <table class="table table-hover" id="supplierstable" cellspacing="0" width="100%">
                    <thead>
                        <th width="10%"></th>
                        <th >Supplier Code</th>
                    </thead>
                    <tfoot>
                        <th colspan="2" rowspan="1"><a href="{{url('/settings/suppliers/create')}}" data-toggle='tooltip' data-placement="top" title="Add New Supplier"><i class="fa fa-plus"></i> Add Supplier</a></th>

                    </tfoot>
                
                    <tbody class="hidden">
                        @foreach($suppliers as $supp)
                            <tr>
                                <td style="text-align: center; vertical-align: middle;"><a href="{{url('/settings/suppliers/'.$supp->id.'/edit') }}" data-toggle="tooltip" data-placement="top" title="Click to modify {{$supp->suppcode}}."><img src="{{url('/images/app/details_edit.png')}}"></a></td>
                                <td>{{ $supp->suppcode }}</td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>


        </div>

        

    </div>
@stop


@section('userdefjs')
    <script>
   
        $(document).ready(function() {
            $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(11).addClass('active');
            
            var table = $('#supplierstable')
                .on( 'init.dt', function () {
                    $("#supplierstable tbody").removeClass('hidden');
                    $("#loaderopen").addClass('hidden');
                })
                .DataTable({
                "columns": [
                    {
                        "className":'edit-control',
                        "orderable":      false,
                        "searchable": false
                        
                    },
                    { "data": "suppcode" },
                    ],
                    select: 'single',
                    "order": [[1, 'asc']]
            });

            $("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');

        }); //End Document Ready

        
    </script>
@stop