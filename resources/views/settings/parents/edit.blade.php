@extends("layout")

@section("content")

    <h2 class="thin">Modify Parent SKUs</h2>
    <p class="text-muted">This is the way to modify a Parent SKU. Make sure that the entries are correct. <br><strong><em class="text-danger">Note </em></strong>that changing a parent's Supplier will not change the suppliers in all the orders.
    <br><strong><em class="text-danger">Note </em></strong> also that changing the name of the Parent SKU will also change in all the orders related to this Parent SKU that are not in Existing Orders.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/settings/parents')}}">Parent SKUs</a></li>
        <li class="active">Edit</li>
    </ol>
    <hr>

    <form method="POST" action="{{url('/settings/parents/modify/'.$psku->id) }}" class="text-left" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            <div class="row">
                <div class="col-lg-6">
                    <h3>{{ $psku->parentcode }}</h3>
                </div>
            </div>
            <hr>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    General Information
                    <small><em>contains the basic info of this Parent SKU {{$psku->parentcode}}.</em></small>
                </div>
                
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Date Created
                                    <span class="text-success"><small><em>
                                        DD-MM-YYYY format, today.
                                    </em></small></span>
                                </label>
                                <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ date('d-m-Y', strtotime($psku->created_at)) }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Parent SKU
                                    <span class="text-success"><small><em>
                                        name of this Parent SKU
                                    </em></small></span>

                                </label>
                                <input type="text" class="form-control input-sm" name="parentcode" autofocus="" value="{{$psku->parentcode}}">
                            </div>
                        </div>   

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Supplier
                                    <span class="text-success"><small><em>
                                        current supplier assigned to this parent
                                    </em></small></span>
                                </label>
                                <select name="suppcode" class="form-control input-sm">
                                    @foreach($suppliers as $supp)
                                        <option value="{{$supp->suppcode}}" {{ ($supp->suppcode == $psku->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>                     

                    </div>

                </div> <!-- ./Panel-body -->
                    
            </div> <!-- ./Panel -->

            <div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <button class="btn btn-primary btn-block" type="submit">Save</button>
                    </div>
                    <div class="col-md-4 pull-right">
                        <a class="btn btn-default btn-block" href="{{url('/settings/parents')}}">Cancel</a>
                    </div>
                </div>
            </div>
    </form>
@stop


@section('userdefjs')
    <script>
        
        $(function(){

            $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(12).addClass('active');
        });
    </script>
@stop