<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/json/generate/all', 'SupplierordersController@generateAllJson');
Route::get('/json/generate/parts', 'PartsController@generate');
Route::get('/json/generate/supplierorders', 'SupplierordersController@generate');
Route::get('/json/generate/specialorders', 'SpecialordersController@generate');
Route::get('/json/generate/existingorders', 'ExistingordersController@generate');
Route::get('/json/generate/backorders', 'BackordersController@generate');

Route::get('/', 'PartsController@index');
Route::get('/home', 'PartsController@index');
Route::get('/parts', 'PartsController@index');
Route::get('/partspms', 'PartsController@index');

Route::get('/partspms/create', 'PartsController@create');
Route::post('/partspms/create/new', 'PartsController@saveNewPart');
Route::get('/partspms/{part}/edit', 'PartsController@edit');
Route::patch('/partspms/modify/{part}', 'PartsController@saveUpdateMain');

Route::get('partspms/getimage/{part}', 'PartsController@getImage');


Route::get('/supplierorders', 'SupplierordersController@index');
Route::get('/supplierorders/getSimpleOrders/{suppo}', 'SupplierordersController@getSimpleOrders');
Route::post('/supplierorders/orderstoexisting', 'SupplierordersController@toexisting');
Route::get('/supplierorders/supplier/change/{suppo}', 'SupplierordersController@viewchangesupplier');
Route::patch('/supplierorders/supplier/update/{suppo}', 'SupplierordersController@updateSupplier');

Route::get('/specialorders', 'SpecialordersController@index');
Route::get('/specialorders/create', 'SpecialordersController@create');
Route::post('/specialorders/create/new', 'SpecialordersController@saveNewSpO');
Route::get('/specialorders/{spo}/edit', 'SpecialordersController@edit');
Route::patch('/specialorders/modify/{spo}', 'SpecialordersController@saveUpdateMain');

Route::get('/specialorders/getimage/{spo}', 'SpecialordersController@getImage');


Route::get('/existingorders', 'ExistingordersController@index');
Route::get('/existingorders/{exOrd}/edit', 'ExistingOrdersController@edit'); //existingorders/{{$exOrd->id}}
Route::patch('/existingorders/modify/{exOrd}', 'ExistingOrdersController@saveUpdateMain'); //existingorders/{{$exOrd->id}}
Route::get('/existingorders/getSubsequentDetails/{exOrd}', 'ExistingOrdersController@getOrderKabits');
Route::get('/existingorders/addso/{exOrd}', 'ExistingordersController@additionalSOFrom');
Route::post('/existingorders/additionalorders/{exOrd}', 'ExistingordersController@additionalOrders');
Route::post('/existingorders/{exOrd}/pendingize', 'ExistingordersController@topending');



Route::get('/backorders', 'BackordersController@index');
Route::get('/backorders/create', 'BackordersController@create');\
Route::post('/backorders/create/new', 'BackordersController@saveNewBko');
Route::get('/backorders/{bko}/edit', 'BackordersController@edit');
Route::patch('/backorders/modify/{bko}', 'BackordersController@saveUpdateMain');


Route::get('/openboxes', 'OpenboxesController@index');
Route::get('/openboxes/create', 'OpenboxesController@create');
Route::post('/openboxes/create/new', 'OpenboxesController@saveNewOPB');
Route::get('/openboxes/{opb}/edit', 'OpenboxesController@edit');
Route::patch('/openboxes/modify/{opb}', 'OpenboxesController@saveUpdateMain');
Route::get('/openboxes/{opb}/clone', 'OpenBoxesController@cloneview');
Route::post('/openboxes/create/clone', 'OpenBoxesController@createclone'); 

Route::get('/raps', 'RapsController@index');
Route::get('/raps/create', 'RapsController@create');
Route::post('/raps/create/new', 'RapsController@saveNewRap');
Route::get('/raps/{rma}/edit', 'RapsController@edit');
Route::patch('/raps/modify/{rma}', 'RapsController@saveUpdateMain');
Route::get('/raps/{rma}/clone', 'RapsController@cloneview');
Route::post('/raps/create/clone', 'RapsController@saveNewRap'); 

Route::get('/import', 'DataRoutesController@import');
Route::post('/import/uploadcsv', 'CSVController@uploadfile');

Route::get('/export', 'DataRoutesController@export');
Route::get('/export/parts', 'DataRoutesController@exportparts');
Route::get('/export/backorders', 'DataRoutesController@exportbackorders');
Route::get('/export/specialorders', 'DataRoutesController@exportspecialorders');
Route::get('/export/openboxes', 'DataRoutesController@exportopenboxes');
Route::get('/export/raps', 'DataRoutesController@exportraps');
Route::get('/export/supplierorders/{exOrd}', 'DataRoutesController@exportsupplierorders');
Route::get('/export/existingorders', 'DataRoutesController@exportexistingorders');
Route::get('/export/existingorders/persku', 'DataRoutesController@exportexistingorderspersku');

//Login
Route::get('logout', 'Auth\LoginController@logout');
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Auth::routes();

Route::patch('/partspms/netoupdate/{part}', 'PartsController@neto');
Route::patch('/partspms/mageupdate/{part}', 'PartsController@mage');
Route::patch('/partspms/wiseupdate/{part}', 'PartsController@wise');

//SETTINGS
Route::get('/settings/parents', 'SettingsController@parentsindex');
Route::get('/settings/parents/{psku}/edit', 'SettingsController@parentsedit');
Route::patch('/settings/parents/modify/{psku}', 'SettingsController@parentupdate');
Route::get('/settings/parents/create', 'SettingsController@parentcreate');
Route::post('/settings/parents/create/new', 'SettingsController@parentcreatenew');

Route::get('/settings/suppliers', 'SettingsController@suppliersindex');
Route::get('/settings/suppliers/{supplier}/edit', 'SettingsController@suppliersedit');
Route::patch('/settings/suppliers/modify/{supplier}', 'SettingsController@suppliersupdate');
Route::get('/settings/suppliers/create', 'SettingsController@supplierscreate');
Route::post('/settings/suppliers/create/new', 'SettingsController@supplierscreatenew');

Route::get('/settings/users', 'SettingsController@usersindex');
Route::get('/settings/users/{user}/edit', 'SettingsController@usersedit'); 
Route::patch('/settings/users/modify/{user}', 'SettingsController@usersupdate');
Route::get('/settings/users/create', 'SettingsController@userscreate');
Route::post('/settings/users/create/new', 'SettingsController@userscreatenew');

Route::get('/settings/user/pw', 'SettingsController@userchangepw');
Route::patch('/settings/users/changecredentials/{user}', 'SettingsController@changemypassword');

Route::get('/reports/fourteendayreport', 'ReportsController@load14dReport');
Route::get('/reports/fourteendayreport/export', 'ReportsController@export14dreport');
//reports/fourteendayreport/export



//ARCHIVING
Route::get('/settings/archive/opb','ArchivesController@opbindex');
Route::post('/settings/archive/archiveselected', 'ArchivesController@archiveselected');
Route::get('/settings/archive/unarchive/opb','ArchivesController@opbunarchive');
Route::post('/settings/archive/unarchive/opbunarchive', 'ArchivesController@unarchive');

Route::get('/settings/archive/rap','ArchivesController@rapindex');
Route::post('/settings/archive/archiveselectedrap', 'ArchivesController@archiveselectedRap');
Route::get('/settings/archive/unarchive/rap','ArchivesController@rapunarchive');
Route::post('/settings/archive/unarchive/rapunarchive', 'ArchivesController@unarchiveRap');


// AUTOMATIONS
Route::get('/automation/import', 'AutomationsController@automationimportview');

Route::post('/automation/import/uploadcsv', 'AutomationsController@uploadfile');


Route::get('/automation/photos/rename', 'AutomationsController@renamephotos');
Route::get('/automation/stocks/status', 'AutomationsController@updateStockStatus');
Route::get('/automation/queries/somethingWrongWithExistingOrders', 'AutomationsController@listAllExistingOrdersWithOutSupplierOrders');


Route::get('/parts/testfile/{part}', 'PartsController@checkFileExists');