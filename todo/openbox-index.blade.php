@extends('layout')

@section('content')
	
		<h2 class="thin">Open Boxes</h3>
		<p class="muted">Open Boxes here. Please close them when you leave.</p>
		
		<ol class="breadcrumb text-left">
			<li class="active">Openboxes</li>
		</ol>

		<hr>

		<div class="text-left">
			<div id="loaderopen" class="text-center">
				<img src="{{url('/images/app/rolling.gif')}}">
			</div>
			<table class="table table-hover" id="opbtable" cellspacing="0" width="100%">
				<thead>
					<th width="3%"></th>
					<th width="4%"></th>
					<th width="4%"></th>
					<th >Date Created</th>
                    <th >Reference Number</th>
                    <th >Owner</th>
                    <th >Parent SKU</th>
                    <th >Supplier</th>
                    <th >Part Removed</th>
                    <th >Qty</th>
                    <th >Location</th>
                    <th >Opb Status</th>
                    <th >Order Status</th>

                    <th >Descr</th>
                    <th >SP Id</th>
                    <th>ID</th>
				</thead>
				<tfoot>
					<th colspan="15" rowspan="1"><a href="{{ url('/openboxes/create') }}" data-toggle='tooltip' data-placement="top" title="Open another box."><i class="fa fa-plus"></i> New Openbox</a></th>

				</tfoot>
			
				<tbody class="hidden">
					
				</tbody>
			</table>

			

		</div>

		

	</div>
@stop


@section('userdefjs')
	<script>
		function getImage(obj){
			
			mydata = "";
			myModalData = "";
			$.ajax({
				url: '/parts/public/openboxes/getimage/' + obj,
				type: 'GET',
				success: function(data){
					console.log(data);
					mydata  += "<a href='' data-toggle='modal' data-target='#myModal'><img src='data:image/jpeg;base64," + data + "' alt='Photo' class='img-thumbnail' width='100' height='100'></a>";

					myModalData  += "<img src='data:image/jpeg;base64," + data + "' alt='Photo' class='img-responsive'>";
					
					$(".appendhere").append(mydata);
					$(".appendheremodal").empty().append(myModalData);

				}
			});
			

			return mydata;
		}

		var removeElements = function(text, selector) {
		    var wrapped = $("<div>" + text + "</div>");
		    
		    return wrapped.text();
		}

		function format(d){
			return '<table class="table table-default" width="40%">'+
		        '<tr class="info">'+
		            '<td width="10%"><strong>' + removeElements(d.partsku,"a") +'</strong></td>'+
		            '<td width="40%"><em></em></td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Location:</td>'+
		            '<td>' + d.whloc +'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>SP Id:</td>'+
		            '<td>'+ d.spid +'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Image Thumbnail:</td>'+
		            '<td class="appendhere"></td>'+
		        '</tr>'+
		    '</table>';
		}


		$(document).ready(function() {
			$(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(3).addClass('active');
			
			var table = $('#opbtable')
				.on( 'init.dt', function () {
			        $("#opbtable tbody").removeClass('hidden');
			        $("#loaderopen").addClass('hidden');
			    })
				.DataTable({
					"ajax": '/parts/public/json/openboxesordersjson.json',
					"columns": [
			            {
			                "className":'details-control',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ''
			            },
			            {
			                "className":'edit-control',
			                "orderable":      false,
			                "searchable": false,
			                "data": null,
			                "render": function ( data, type, full, meta ) {
						      	return "<td style='text-align: center; vertical-align: middle;'><a href='/parts/public/openboxes/"+data.id+"/edit' data-toggle='tooltip' data-placement='top' title='Click to modify.'><img src='/parts/public/images/app/details_edit.png'></a></td>";
						    }
			                
			            },
			            {
			                "className":'clone-control',
			                "orderable":      false,
			                "searchable": false,
			                "data": null,
			                "render": function ( data, type, full, meta ) {
								      	return "<td style='text-align: center; vertical-align: middle;'><a href='/parts/public/openboxes/"+data.id+"/clone'  width='3%'  data-toggle='tooltip' data-placement='top' title='Clone "+data.partsku+"?'><img src='/parts/public/images/app/clone.gif'></a></td>";
								    }
			                
			            },
			            { "data": "created_at" },
			            { "data": "referencenum" },
			            { 
			            	"data": "sys_addedby"
			             },
			            { "data": "parentcode" },
			            { "data": "suppcode" },
			            { "data": "partsku" },
			            { "data": "qty" },
			            { "data": "whloc" },
			            { "data": "sys_orderstatus" },
			            { "data": "sys_finalstatus" },

			            { "data": "descr" },
			            { "data": "spid" },
			            { "data": "id" },
			            ],
			        "columnDefs": [
			            {
			                "targets": [ 13, 14, 15 ],
			                "visible": false,
			                "searchable": false
			            }

			        	],
			        	select: 'single',
			            "order": [[1, 'asc']]
			});


			$('#opbtable tbody').on('click', 'td.details-control', function () {
			    var tr = $(this).closest('tr');
			    var row = table.row( tr );
			 
			    if ( row.child.isShown() ) {
			        // This row is already open - close it
			        row.child.hide();
			        tr.removeClass('shown');
			    }
			    else {
			        if ( table.row( '.shown' ).length ) {
						$('.details-control', table.row( '.shown' ).node()).click();
					}

					row.child( format(row.data()) ).show();
					tr.addClass('shown');
					getImage(row.data().photo);
			    }
			});


			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');
			$("th.clone-control").removeClass('sorting_asc').addClass('sorting_disabled');



		}); //End Document Ready

		
	</script>
@stop