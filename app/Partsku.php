<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partsku extends Model
{
    protected $guarded = array(); 
    
    public function supplier (){
    	return $this->hasOne(Supplier::class);
    }

    public function stocks (){
    	return $this->hasMany(Stock::class);
    }


    public function stocksNotCancelledReceivedInTransit (){
        return $this->hasMany(Stock::class)
            ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"]);
    }

    /*public function supplierorders (){
    	return $this->belongsTo(Supplierorder::class, "supplierorder_id", "id");
    }*/
}
