<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    //

    public function partskuIbelong (){
    	return $this->belongsTo(Partsku::class, "partsku_id", "id");
    }

    public function supplierorder (){
    	return $this->belongsTo(Supplierorder::class);
    }

    public function lasttouch (){
        return $this->belongsTo(User::class, "sys_lasttouch", "id");
    }
}
