<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Storage;
use Carbon;

use App\Parentsku;
use App\Supplier;
use App\Specialorder;
use App\Supplierorder;
use App\Backorder;

class BackordersController extends RootController
{
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Generate JSON File
     *
     * @return void
     */
    public function generate(){
        return $this->backordersGenerateJson();
	}


	/**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function index (){
        return view("backorders.index");
    }



    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function create(){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();
        return view("backorders.create", compact('parents', 'suppliers'));
    }



        /**
     * Save New SpOs to Supplier Orders
     * @param Request $request The variables sent by the user for creation
     * @return void
     */
    public function forSupplierOrders(Request $request, Backorder $bko){
            $thereExistsSuppOrd = Supplierorder::where("partsku", $request->partsku)
                ->where("suppcode", $request->suppcode)
                ->where("parentcode", $request->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            if (!is_null($thereExistsSuppOrd)) { // if there exist same partsku in Supplier Order
                // There should only be ONE Pending at a time
                if ($bko->supplierorder_id != $thereExistsSuppOrd->id) {
                    $thereExistsSuppOrd->backorder += $request->qty;
                    $thereExistsSuppOrd->save();
                }else{
                    $thereExistsSuppOrd->backorder = $request->qty;
                    $thereExistsSuppOrd->save();
                }
                
                $bko->supplierorder_id = $thereExistsSuppOrd->id;
                $bko->sys_finalstatus = "Pending";
                $bko->save();
                

                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $newSuppOrd = new Supplierorder;
                $newSuppOrd->partsku = $request->partsku;
                $newSuppOrd->descr = $request->descr;
                $newSuppOrd->parentcode = $request->parentcode;
                $newSuppOrd->suppcode = $request->suppcode;
                $newSuppOrd->backorder = $request->qty;
                $newSuppOrd->sys_finalstatus = "Pending";

                $newSuppOrd->sys_addedby = Auth::user()->id;
                $newSuppOrd->sys_isactive = 1;
                $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
                
                $newSuppOrd->save();

                $bko->sys_finalstatus = "Pending";
                $bko->supplierorder_id = $newSuppOrd->id;
                $bko->save();

                $this->recomputeSupplierOrderTotals($newSuppOrd);
            }
    }




    /**
     * Save the Newly created Special Order
     * @todo Function for those that needs Ordering - how to make new Partsku go to Supplier Orders
     * @param Request $request the Part created by the user
     * @return void
     */
    public function saveNewBko (Request $request){
        $newBko = new Backorder;

        $newBko->dateinvoiced = \Carbon\Carbon::now()->format('Y-m-d');

        //dd($newBko->dateinvoiced, \Carbon\Carbon::now()->format('Y-m-d'));

        $newBko->ordernum = $request->ordernum;
        $newBko->partsku = $request->partsku;
        $newBko->descr = $request->descr;
        $newBko->ordertype = $request->ordertype;
        $newBko->category = $request->category;
        $newBko->spid = $request->spid;

        $newBko->parentcode = $request->parentcode;
        $newBko->suppcode = $request->suppcode;
        
        $newBko->eta = date('Y-m-d', strtotime($request->eta));
        $newBko->qty = $request->qty;
        $newBko->remarks = $request->remarks;
        
        $newBko->sys_orderstatus = $request->sys_orderstatus;
        $newBko->sys_finalstatus = $request->sys_finalstatus;
        $newBko->sys_addedby = Auth::user()->id;
        $newBko->sys_lasttouch = Auth::user()->id;        
        $newBko->sys_isactive = 1; //Active always
    
        $newBko->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
        }

        $newBko->save();

        //$this->forSupplierOrders($request, $newBko);
        $this->backordersGenerateJson();
        $this->supplierordersGenerateJson();

        return redirect("/backorders");
    }



    /**
     * Ajax to get image from storage
     * @param Specialorder $spo the Partsku instance
     * @return base 64 encoded image
     */
    public function getImage (Backorder $bko){
        return base64_encode(Storage::get('/public/' . $bko->photo));
    }




    /**
     * Load View for Editing Special Orders
     * @param Specialorder $special the SPO instance
     * @return List of Parents, Suppliers, the Part SKU instance, Stocks related to the Part SKU and the Current Stock
     */
    public function edit(Backorder $bko){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();

        return view("backorders.edit", compact('parents', 'suppliers', 'bko'));
    }


    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function saveUpdateMain(Request $request, Backorder $bko){
        

        if ($request->sys_finalstatus == "Cancelled" or $request->sys_finalstatus == "Unresolved") {
                if ( !empty($bko->supplierorder_id )) {
                    $thereExistsSuppOrd = Supplierorder::where("id", $bko->supplierorder_id)
                        ->first();

                    if (!empty($thereExistsSuppOrd)) { //just a precaution
                        $thereExistsSuppOrd->backorder = (intval($thereExistsSuppOrd->backorder) - intval($bko->qty));
                        $thereExistsSuppOrd->save();

                        $bko->supplierorder_id = 0;
                        $bko->sys_lasttouch = Auth::user()->id;
                        $bko->save();
                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }

                    if ($thereExistsSuppOrd->sys_total == 0) {
                        $thereExistsSuppOrd->sys_finalstatus = ($request->sys_finalstatus == "Cancelled" ? "Cancelled" : "Unresolved");
                        $thereExistsSuppOrd->sys_isactive = 0;
                        $thereExistsSuppOrd->save();
                    }

                }else{ // No Supplier Order entry
                    
                    $bko->sys_finalstatus = $request->sys_finalstatus;
                    $bko->sys_lasttouch = Auth::user()->id;
                    $bko->save();
                }
        }else if($request->sys_finalstatus == "Pending"){ //Pending pa rin

                $thereExistsSuppOrd = Supplierorder::where("id", $bko->supplierorder_id)
                        ->first();

                $anotherSuppOrd = Supplierorder::where("partsku", $request->partsku)
                    ->where("suppcode", $request->suppcode)
                    ->where("parentcode", $request->parentcode)
                    ->where("sys_isactive", "1")
                    ->where("sys_finalstatus", "Pending")
                    ->first();


                if ($request->parentcode != "0" && $request->suppcode != "0") {

                    //dd("Parentcode != 0 and Suppcode != 0", $thereExistsSuppOrd, $anotherSuppOrd);

                    if (!empty($thereExistsSuppOrd)) { //just a precaution
                        //dd("TOP", $thereExistsSuppOrd);

                        if ($request->parentcode == $thereExistsSuppOrd->parentcode && $request->suppcode == $thereExistsSuppOrd->suppcode) {
                            
                            $thereExistsSuppOrd->backorder = (intval($thereExistsSuppOrd->backorder) - intval($bko->qty)) + intval($request->qty);
                            $thereExistsSuppOrd->save();

                            $bko->sys_finalstatus = $request->sys_finalstatus;
                            $bko->save();

                        }else{
                             // there has been a change in either supplier or parentcode
                            // if anotherSuppOrd is not empty, attach to that suppord
                            // else, create a new supplierorder


                            if (!empty($anotherSuppOrd)) {
                                $anotherSuppOrd->backorder += intval($request->qty);
                                $anotherSuppOrd->save();

                                $this->recomputeSupplierOrderTotals($anotherSuppOrd);

                                $bko->supplierorder_id = $anotherSuppOrd->id;
                                $bko->sys_finalstatus = "Pending";
                                $bko->save();

                                $thereExistsSuppOrd->backorder = intval($thereExistsSuppOrd->backorder) - intval($bko->qty);
                                $thereExistsSuppOrd->save();

                                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

                                if (intval($thereExistsSuppOrd->sys_total) == 0) {
                                    $thereExistsSuppOrd->sys_finalstatus = "Cancelled";
                                    $thereExistsSuppOrd->save();    
                                }

                            }else{

                                $newSuppOrd = new Supplierorder;
                                $newSuppOrd->partsku = $request->partsku;
                                $newSuppOrd->descr = $request->descr;
                                $newSuppOrd->parentcode = $request->parentcode;
                                $newSuppOrd->suppcode = $request->suppcode;
                                $newSuppOrd->specialorder = $request->qty;
                                $newSuppOrd->sys_finalstatus = "Pending";

                                $newSuppOrd->sys_addedby = Auth::user()->id;
                                $newSuppOrd->sys_isactive = 1;
                                $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
                                
                                $newSuppOrd->save();

                                $bko->sys_finalstatus = "Pending";
                                $bko->supplierorder_id = $newSuppOrd->id;
                                $bko->save();

                                $this->recomputeSupplierOrderTotals($newSuppOrd);

                            }
                        }

                        //dd($thereExistsSuppOrd->specialorder . "; " . $spo->qty . "; ". $request->qty . ";");

                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }else{ // no supplierorder pa, meaning, tie in if there is a anotherSuppOrd
                        //dd("BOTTOM", $thereExistsSuppOrd);

                        if (!empty($anotherSuppOrd)) {
                            $anotherSuppOrd->backorder += intval($request->qty);
                            $anotherSuppOrd->save();

                            $this->recomputeSupplierOrderTotals($anotherSuppOrd);

                            $bko->supplierorder_id = $anotherSuppOrd->id;
                            $bko->sys_finalstatus = "Pending";
                            $bko->save();
                        }else{
                            $this->forSupplierOrders($request, $bko);
                        }
                    }

                }else{
                    $bko->sys_finalstatus = "Unresolved";
                    $bko->save();
                }
        }

        $bko->update($request->except(
            'created_at', 'photo', 'sys_finalstatus', 'dateinvoiced'
            ));
        $bko->sys_lasttouch = Auth::user()->id;
        $bko->save();

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));

            $bko->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        }
        $bko->save();

        if ($request->parentcode == "0" or $request->suppcode == "0") {
            if ($request->sys_finalstatus == "Cancelled") {
                $bko->sys_finalstatus = "Cancelled";
            }else{
                $bko->sys_finalstatus = "Unresolved";
            }
            
            $bko->save();
        }

        $this->generate();
        $this->supplierordersGenerateJson();
       
        return redirect('/backorders');
    }
}
