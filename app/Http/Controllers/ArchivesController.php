<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Openbox;
use App\Rap;
use Session;

class ArchivesController extends RootController
{
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function opbindex (){
    	$openboxes = Openbox::select("id", "created_at", "referencenum", "parentcode", "suppcode", "partsku", "qty", "whloc", "sys_orderstatus", "sys_finalstatus", "descr", "fault", "spid", "sys_addedby")
    		->whereIn("sys_finalstatus", ["Cancelled", "Received"])
    		->where("is_archived", 0)
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $openboxes->toArray());
        
        file_put_contents("json/forarchiveopb.json", json_encode($da));
        return view("archive.openboxes");
    } //archivedopb


    /**
     * Load View for Un-Archiving List
     *
     * @return void
     */
    public function opbunarchive (){
    	$openboxes = Openbox::select("id", "created_at", "referencenum", "parentcode", "suppcode", "partsku", "qty", "whloc", "sys_orderstatus", "sys_finalstatus", "descr", "fault", "spid", "sys_addedby")
    		->where("is_archived", 1)
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $openboxes->toArray());
        
        file_put_contents("json/archivedopb.json", json_encode($da));
        return view("archive.archived_openboxes");
    } //archivedopb

    
    /**
     * Archive Selected
     *
     * @return void
     */
    public function archiveselected (Request $request){
    	$product_name = "";
        foreach (json_decode($request->selected_archiveopb) as $row) {
            $opb = Openbox::find($row->id);
            $opb->is_archived = 1;
            $opb->save();
            $product_name .= $opb->partsku . ",";
        }

        $this->openboxesordersGenerateJson();

        $request->session()->flash('there-has-been-an-archived-openbox-message', 'Archiving ' . rtrim($product_name,',') .' Successful!');

        return redirect("/settings/archive/opb");

    }


    /**
     * Un-Archive Selected
     *
     * @return void
     */
    public function unarchive (Request $request){
    	$product_name = "";

        foreach (json_decode($request->selected_archiveopb) as $row) {
            $opb = Openbox::find($row->id);
            $opb->is_archived = 0;
            $opb->save();
            $product_name .= $opb->partsku . ",";
        }

        $this->openboxesordersGenerateJson();
        $request->session()->flash('there-has-been-an-un-archived-openbox-message', 'Returned ' . rtrim($product_name,',') .' to Un-archived list!');

        return redirect("/settings/archive/unarchive/opb");

    }



    /**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function rapindex (){
        $raps = Rap::select("id", "created_at", "rmanum", "parentcode", "suppcode", "partsku", "descr", "kayakoid", "whloc", "qty", "sys_orderstatus", "sys_finalstatus")
            ->where("is_archived", 0)
            ->whereIn("sys_finalstatus", ["Cancelled", "Received"])
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $raps->toArray());
               
        file_put_contents("json/forarchiverap.json", json_encode($da));
        
        return view("archive.raps");
    } //archivedopb


    /**
     * Archive Selected RAP
     *
     * @return void
     */
    public function archiveselectedRap (Request $request){
        $product_name = "";
        foreach (json_decode($request->selected_archiverap) as $row) {
            $rap = Rap::find($row->id);
            $rap->is_archived = 1;
            $rap->save();
            $product_name .= $rap->partsku . ",";
        }

        $this->rapsordersGenerateJson();

        $request->session()->flash('there-has-been-an-archived-rap-message', 'Archiving RAP ' . rtrim($product_name,',') .' Successful!');

        return redirect("/settings/archive/rap");

    }


    /**
     * Load View for Un-Archiving List
     *
     * @return void
     */
    public function rapunarchive (){
        $raps = Rap::select("id", "created_at", "rmanum", "parentcode", "suppcode", "partsku", "descr", "kayakoid", "whloc", "qty", "sys_orderstatus", "sys_finalstatus")
            ->where("is_archived", 1)
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $raps->toArray());
        
        file_put_contents("json/archivedrap.json", json_encode($da));
        return view("archive.archived_raps");
    } //opbunarchive


    /**
     * Un-Archive Selected
     *
     * @return void
     */
    public function unarchiveRap (Request $request){
        $product_name = "";

        foreach (json_decode($request->selected_archiverap) as $row) {
            $rap = Rap::find($row->id);
            $rap->is_archived = 0;
            $rap->save();
            $product_name .= $rap->partsku . ",";
        }

        $this->rapsordersGenerateJson();

        $request->session()->flash('there-has-been-an-un-archived-rap-message', 'Returned ' . rtrim($product_name,',') .' to Un-archived list!');

        return redirect("/settings/archive/unarchive/rap");

    }
}
