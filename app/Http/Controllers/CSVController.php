<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Partsku;
use App\Parentsku;
use App\Supplier;
use App\Stock;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Backorder;
use App\Openbox;
use App\Rap;

use Schema;
use Carbon\Carbon;
use DB;
use Auth;

class CSVController extends CSVRootController
{	
    
    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function backordersGenerateJson(){

        $backorders = Backorder::select("id", 'dateinvoiced', "ordernum", "parentcode", "suppcode", "partsku", 
            "descr", "ordertype", "category", "eta", "qty", "remarks", "spid", "sys_orderstatus", "sys_finalstatus")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $backorders->toArray());
               
        file_put_contents("json/backordersjson.json", json_encode($da));

        return "Back Orders Data Generated.";
    }

    /**
     * Generates a Json file for Parts.index
     *
     * @return json
     */
    public function partsGenerateJson(){

        $parts = Partsku::join("stocks", "stocks.partsku_id", "partskus.id")
            ->where("stocks.sys_iscurrent", "1")
            ->select("partskus.id", "partskus.partsku", "partskus.suppcode", "partskus.parentcode", "partskus.oldsku", "partskus.whloc", "partskus.sys_isactive", "partskus.descr", "stocks.soh", "partskus.barcode", "stocks.restockqty", "stocks.reorderqty", "stocks.sys_status","partskus.inneto","partskus.inmagento","partskus.inwise", "stocks.sys_finalstatus")
            ->orderBy("partskus.updated_at", "partskus.created_at", "partskus.partsku")
            ->get();

        $da = array("data" => $parts->toArray());
       
        file_put_contents("json/partsjson.json", json_encode($da));

        return "Data Generated.";
    }





     public function uploadfile (Request $request) {
	    $orderTyp = $request->importtype;
      	$redirecthere = "parts";

		$useThisTable = $this->whatTableToUse($orderTyp);
		
        $file = $request->image;
        $filepath = storage_path('app/csv/'.$file->getClientOriginalName());
        $extension = $file->getClientOriginalExtension();

        $request->image->move(storage_path('app/csv'), $request->image->getClientOriginalName());
	        
        //1. Get Header of CSV File
      	$csvHeaders = $this->getCsvHeaders($filepath);

	      //dd(count($csvHeaders));
		if (count($csvHeaders) === 0) {
			abort(500,"CSV Headers not found. Rectify the issue and please try again.");
		}

		//2. Get list of columns in table tempPartsku
		$colsTestTable = Schema::getColumnListing($useThisTable); 

		//dd($colsTestTable);
		//3. Check what tables are present  against the headers from the CSV File
		//$liveHeaders = $this->getMatchingHeaders($colsTestTable, $csvHeaders);
		$liveHeaders = array_intersect($colsTestTable, $csvHeaders);

		if (count($liveHeaders)===0) {
			abort (500, "CSV Headers did not match any of the Column Tables. Download the template just to be sure and try again.");
		}
		
		//4. Check the difference between CSV headers and those that match from the database
		$errorHeaders = array_udiff($csvHeaders, $liveHeaders, 'strcasecmp');

		//5. If there are error in headers, ask user to rectify CSV file. Exit.
		if (count($errorHeaders) > 0) {
			$erroneousHeaders = implode(", ", $errorHeaders);
			abort (500, "Some CSV headers did not match any of the Column Tables. <br>\n# of Columns: " . count($errorHeaders) . "\nErroneous Headers: " . $erroneousHeaders);
		}

		$sqlTables = "";
		$sqlTables .= "(" . implode(", ", $csvHeaders) . ")";

		/* ----------------------------
		  SQL FUNCTIONS
		-------------------------------*/
		//1. Delete all data from TEMP first
		$this->truncateTemp($orderTyp);


		//2. Load Data From CSV to MySQL Table; PartSKU is a required field

		//These are the required fields
		//List here the required fields for each table
		$stockHeaders = array("partsku", "soh", "restockqty", "reorderqty", "needsordering");
		$piHeaders = array("partsku", "parent", "supplier");

		$bkoHeaders = array("partsku", "ordernumber");

		$proceed = false;
		$proceedArr = $stockHeaders;
		if ($orderTyp != "6") {
			
			// Check the required fields if the required fields are in the csv headers
			switch ($orderTyp) {
				case '7': //Stocks Import
						$proceed = (count(array_intersect($stockHeaders, $liveHeaders)) == count($stockHeaders) ? true : false);
						$proceedArr = $stockHeaders;

					break;

				case '1': //PI Import
						$proceed = (count(array_intersect($piHeaders, $liveHeaders)) == count($piHeaders) ? true : false);
						$proceedArr = $piHeaders;

					break;

				case '2': //BKO Import

						$proceed = (count(array_intersect($bkoHeaders, $liveHeaders)) == count($bkoHeaders) ? true : false);
						$proceedArr = $bkoHeaders;

						
					break;
				
				default:
					# code...
					break;
			}
			
			if ($proceed) {
		      	$query = "LOAD DATA LOCAL INFILE '" . addslashes($filepath) . "' INTO TABLE ". $useThisTable ." COLUMNS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES " . $sqlTables;
		        
		      	\DB::connection()->getpdo()->exec($query);
		      
		  	}else{
		    	abort (500, "CSV Headers Error, [".implode(", ", $proceedArr)."]  are all required fields. Please provide the required columns.");
		  	}
			  	

		}else{
			  $query = "LOAD DATA LOCAL INFILE '" . addslashes($filepath) . "' INTO TABLE ". $useThisTable ." COLUMNS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES " . $sqlTables;
			        
			  \DB::connection()->getpdo()->exec($query);
		}

		//3. Determine what to import:
		switch ($orderTyp) {
			case '1': 
				$this->updateIsActiveForTempPartsku($liveHeaders);
				$this->updateInDbForTempPartsku();
				$this->updateAuthorForTempPartsku();

				$this->processSysInDb1Partsku();
				$this->partsGenerateJson();

				$redirecthere = "/parts";
				break;

			case '7':
				$this->updateIsActiveForTempPartsku($liveHeaders);
				$this->updateInDbForTempPartsku();
				$this->updateAuthorForTempPartsku();

				$this->processSysInDb1Stock();

				$this->partsGenerateJson();
				
				$redirecthere = "/parts";
				break;


			case '2':

				$this->updateIsActiveForTempOrders();
				$this->updateAuthorForTempOrders();
				$this->processBackOrder();

				$this->backordersGenerateJson();

				$redirecthere = "/backorders";
				break;

			default: 
				$redirecthere = "/home";
				break;
		}
				
				

		
        return Redirect::to($redirecthere);

	} //end import file
}
