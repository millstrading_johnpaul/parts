<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Partsku;
use App\Parentsku;
use App\Supplier;
use App\Stock;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Openbox;
use App\Rap;
use App\Backorder;

use Auth;
use DB;
use Carbon\Carbon;
use Excel;

class ReportsController extends Controller
{
    //



    public function load14dReport (){

        $from = Carbon::now()->subDays(13);
        
        $ponums = Existingorder::where("orderavenue", "DHL")
            ->where("created_at", "<=", $from)
            ->whereIn("sys_finalstatus", ["Pending with Daisy", "Confirmed with Daisy"])
            ->get();
        
        return view('reports.report14days', compact('ponums'));    
        
    }


    /**
     * Load Export for PI
     *
     * @return void
     */
    public function export14dreport (){
        $from = Carbon::now()->subDays(1);
        
        $ponums = Existingorder::where("orderavenue", "DHL")
            ->where("created_at", "<=", $from)
            ->whereIn("sys_finalstatus", ["Pending with Daisy", "Confirmed with Daisy"])
            ->get();


        Excel::create('14dayReport', function($excel) use ($ponums) {
            $excel->sheet('Sheet1', function($sheet) use ($ponums) {
                $sheet->loadView('reports.xlsx14d')->with("ponums", $ponums);
            });
        })->export('xlsx');

    }


}
