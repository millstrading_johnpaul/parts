<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Partsku;
use App\Parentsku;
use App\Supplier;
use App\Stock;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Backorder;
use App\Openbox;
use App\Rap;
use App\User;

use Auth;
use Carbon;


class SettingsController extends RootController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }


    /**
     * Re-Generate ALL JSON File
     *
     * @return void
     */
    public function generateAllJson(){
        $this->partsGenerateJson();
        $this->supplierordersGenerateJson();
        $this->specialordersGenerateJson();
        $this->existingordersGenerateJson();
        $this->backordersGenerateJson();
        $this->openboxesordersGenerateJson();
        $this->rapsordersGenerateJson();

        //return "All Data Generated.";
    }



    /**
     * redirect to view all Parents SKUs.
     *
     * @return the parents view
     */
    public function parentsindex (){
        $parentskus = Parentsku::where("parentcode", "<>", "0")
            ->orderBy("parentcode")
            ->get();
        return view('settings.parents.index', compact('parentskus'));
    }


    /**
     * redirect to edit a certain Parents SKU.
     *
     * @return the parent view for editing
     */
    public function parentsedit (Parentsku $psku){
        $suppliers = Supplier::where("suppcode", "<>", "0")
            ->orderBy("suppcode")
            ->get();


        return view('settings.parents.edit',compact('psku', 'suppliers'));
    }



    /**
     * Update the Parent SKU.. If the name of the parent SKU has been updated, modify also all those that are in the orders...BUT will not re-match those that are in Supplier Orders
     *
     * @return redirecting to index
     */
    public function parentupdate (Request $request, Parentsku $psku){
    	if ($request->parentcode != $psku->parentcode or $request->suppcode != $psku->suppcode) {

            $toUpdate = Partsku::join("stocks", "stocks.partsku_id", "partskus.id")
                ->whereNotIn("stocks.sys_finalstatus", ["Cancelled", "Received", "In Transit"])
                ->where("partskus.parentcode", $psku->parentcode)
                ->get();
                /*->update(["partskus.parentcode" => $request->parentcode, "partskus.suppcode" => $request->suppcode, "partskus.updated_at"=>\Carbon\Carbon::now(), "stocks.updated_at"=>\Carbon\Carbon::now()]);*/

            foreach ($toUpdate as $value) {
                $value->parentcode = $request->parentcode;
                $value->suppcode = $request->suppcode;
                $value->save();
            }

    		/*Partsku::where("parentcode", $psku->parentcode)
    			->update(["parentcode" => $request->parentcode, "suppcode" => $request->suppcode]);*/

    		Backorder::where("parentcode", $psku->parentcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["parentcode" => $request->parentcode, "suppcode" => $request->suppcode]);

    		Openbox::where("parentcode", $psku->parentcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["parentcode" => $request->parentcode, "suppcode" => $request->suppcode]);

    		Rap::where("parentcode", $psku->parentcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["parentcode" => $request->parentcode, "suppcode" => $request->suppcode]);

    		Specialorder::where("parentcode", $psku->parentcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["parentcode" => $request->parentcode, "suppcode" => $request->suppcode]);

    		Supplierorder::where("parentcode", $psku->parentcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["parentcode" => $request->parentcode, "suppcode" => $request->suppcode]);

            $this->generateAllJson();
    	}
		$psku->update(["parentcode"=>$request->parentcode, "suppcode"=>$request->suppcode]);
    	
        return redirect("/settings/parents");
    }



    /**
     * Create a new parent SKU with a Supplier
     * 
     * @return redirecting to index
     */
    public function parentcreatenew (Request $request){
        $newparent = new Parentsku;

        $newparent->parentcode = $request->parentcode;
        $newparent->suppcode = $request->suppcode;
        $newparent->created_at = date('Y-m-d', strtotime($request->created_at));

        $newparent->save();
        return redirect("/settings/parents");
    }



    /**
     * Create a new parent SKU with a Supplier
     * 
     * @return view to creating a new parent
     */
    public function parentcreate (){
        
        $suppliers = Supplier::where("suppcode", "<>", "0")
            ->orderBy("suppcode")
            ->get();

        return view('settings.parents.create', compact('suppliers'));
    }





    // SUPPLIERS
    /**
     * Index view for Suppliers
     * 
     * @return view to all suppliers
     */
    public function suppliersindex (){
        $suppliers = Supplier::where("suppcode", "<>", "0")
            ->orderBy("suppcode")
            ->get();
        return view('settings.suppliers.index', compact('suppliers'));
    }


    /**
     * Edit view for Suppliers
     * 
     * @return view to edit a particular suppliers
     */
    public function suppliersedit (Supplier $supplier){
        return view('settings.suppliers.edit',compact('supplier'));
    }



    //Updating Supplier will also update all the orders related to the Supplier
    /**
     * Update the Supplier, including those that it Supplies to
     * 
     * @return view to all suppliers
     */
    public function suppliersupdate (Request $request, Supplier $supplier){
    	if ($request->suppcode != $supplier->suppcode) {

            $toUpdate = Partsku::join("stocks", "stocks.partsku_id", "partskus.id")
                ->whereNotIn("stocks.sys_finalstatus", ["Cancelled", "Received", "In Transit"])
                ->where("partskus.suppcode", $supplier->suppcode)
                ->get();
                /*->update(["partskus.parentcode" => $request->parentcode, "partskus.suppcode" => $request->suppcode, "partskus.updated_at"=>\Carbon\Carbon::now(), "stocks.updated_at"=>\Carbon\Carbon::now()]);*/

            foreach ($toUpdate as $value) {
                $value->suppcode = $request->suppcode;
                $value->save();
            }


    		/*Partsku::where("suppcode", $supplier->suppcode)
    			->update(["suppcode" => $request->suppcode]);*/

    		Backorder::where("suppcode", $supplier->suppcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["suppcode" => $request->suppcode]);

    		Openbox::where("suppcode", $supplier->suppcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["suppcode" => $request->suppcode]);

    		Rap::where("suppcode", $supplier->suppcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["suppcode" => $request->suppcode]);

    		Specialorder::where("suppcode", $supplier->suppcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["suppcode" => $request->suppcode]);

    		Supplierorder::where("suppcode", $supplier->suppcode)
    			->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
    			->update(["suppcode" => $request->suppcode]);
    	}
    		$supplier->update(["suppcode"=>$request->suppcode]);
    	
        return redirect("settings/suppliers");
    }



    /**
     * Return view to create Suppliers
     * 
     * @return view to all suppliers
     */
    public function supplierscreate (){
        return view('settings.suppliers.create');
    }


    /**
     * Create Suppliers
     * 
     * @return view to index of all suppliers
     */
    public function supplierscreatenew (Request $request){
        $newSupplier = new Supplier;

        $newSupplier->suppcode = $request->suppcode;
        
        $newSupplier->save();    
        return redirect("settings/suppliers");
       
    }



    //USERS
    /**
     * List all Users
     * 
     * @return view to index of all suppliers
     */
    public function usersindex (){
        $users = User::all();
        return view('settings.users.index', compact('users'));
    }

    /**
     * Edit this specific user
     * 
     * @return view to Edit the User
     */
    public function usersedit (User $user){
        return view('settings.users.edit',compact('user'));
    }

    /**
     * Save this user's update
     * 
     * @return view to Edit the User
     */
    public function usersupdate (Request $request, User $user){
        $user->update($request->except('created_at', 'password', 'remember_token'));

        if (!empty($request->newpassword)) {
        	$user->password = Hash::make($request->newpassword);
        	$user->save();
        }

        return redirect("settings/users");
    }


    /**
     * View for Create a New User
     * 
     * @return view to Create new User
     */
    public function userscreate (){
        return view('settings.users.create');
    }


    /**
     * Create a New User
     * 
     * @return view to Create new User
     */
    public function userscreatenew (Request $request){
        $newuser = new User;

        $newuser->name = $request->name;
        $newuser->email = $request->email;
        $newuser->password = bcrypt($request->password);
        $newuser->accesslevel = $request->accesslevel;
        
        $newuser->save();    
        return redirect("settings/users");
       
    }


    /**
     * I want to change my password!
     * 
     * @return view to Edit the User PW
     */
    public function userchangepw (){
        return view('settings.passw.changepw')->with("userid", Auth::user()->id);
    }

    /**
     * Save this user's change password
     * 
     * @return view to Edit the User
     */
    public function changemypassword (Request $request, User $user){
        if (!empty($request->newpassword)) {
            $user->password = Hash::make($request->newpassword);
            $user->save();
        }

        return redirect("/home");
    }



   

}
