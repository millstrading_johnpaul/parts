<?php

namespace App\Http\Middleware;

use Closure;

class RolesForCS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $loggedIn = $request->user();
        if ($loggedIn->accesslevel == "99" || 
                $loggedIn->accesslevel == "1" ||
                $loggedIn->accesslevel == "2" || 
                $loggedIn->accesslevel == "3" ||
                $loggedIn->accesslevel == "4") {

            return $next($request);    

        //If  CS Team
        }

        abort(404, "CS, No way.");
    }
}
