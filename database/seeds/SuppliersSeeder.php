<?php

use Illuminate\Database\Seeder;

class SuppliersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /*DB::table('suppliers')->insert([
            'suppcode' => 'MIT-SUP'.str_random(4),
            'descr' => str_random(30),
        ]);*/

        factory(App\Supplier::class, 20)->create();/*->each(function ($u) {
	        $u->posts()->save(factory(App\Post::class)->make());
	    });*/
    }
}
