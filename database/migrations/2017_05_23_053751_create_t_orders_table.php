<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('t_orders', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('partsku');

            $table->string('supplier')->nullable();
            $table->string('parent')->nullable();

            $table->text('description')->nullable();
            $table->text('fault')->nullable();
            $table->text('remarks')->nullable();
            $table->text('notes')->nullable();

            $table->string('rmanum')->nullable();
            $table->string('ordernum')->nullable();
            $table->string('referencenum')->nullable();
            $table->string('rma')->nullable();
            $table->string('spid')->nullable();
            $table->string('kayakoid')->nullable();

            $table->string('ordertype')->nullable();
            $table->string('outcome')->nullable();
            $table->string('category')->nullable();
            $table->string('whloc')->nullable();

            $table->integer('qty')->default(0);

            $table->string('dateinvoiced')->nullable();
            $table->string('eta')->nullable();
            
            $table->string('sys_orderstatus')->default("New");
            $table->boolean('sys_isactive')->default(false);
            $table->integer('sys_addedby')->default(0);
            $table->integer('sys_lasttouch')->default(0);
            $table->string('sys_finalstatus')->default("Unresolved");

            $table->boolean('sys_indb')->default(false);

            $table->integer('supplierorder_id')->unsigned()->default(0);

            $table->string('photo')->default('qmbyjpserame.png');

            $table->timestamps();

            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::drop('t_orders');
    }
}
