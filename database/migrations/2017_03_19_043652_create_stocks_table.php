<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('stocks', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('partsku');
            
            $table->integer('partsku_id')->unsigned()->default(0);
            $table->integer('supplierorder_id')->unsigned()->default(0);

            $table->integer('soh')->default(0);
            $table->integer('restockqty')->default(0);
            $table->integer('reorderqty')->default(0);
            $table->integer('salespast150d')->default(0);
            /*$table->integer('qtyinbulkloc')->default(0);*/
            $table->integer('onorderqty')->default(0);

            $table->integer('sys_iscurrent')->default(1); //will identify if this stock record is the most current
            $table->string('sys_status')->nullable(); //Part Status
            $table->integer('sys_addedby')->default(0); //will identify who created this stock record
            $table->integer('sys_lasttouch')->default(0);
            $table->boolean('sys_isactive')->default(false);

           
            $table->string('sys_finalstatus')->default("Unresolved");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('stocks');
    }
}
