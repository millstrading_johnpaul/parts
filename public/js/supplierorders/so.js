
	
	function format(d){
		html = "";
		spo = d.specialordersid;
		no = d.stocksid;
		bko = d.backordersid;
		opb = d.openboxesid;
		rap = d.rapsid;
		

		html = "" + 
			'<div class="col-lg-10 col-lg-offset-1">' + 
				'<caption>' +
					'<a href="'+globalUrl+'/supplierorders/supplier/change/'+d.id+'">'+
						'<i class="fa fa-pencil-square-o"></i> Change Supplier'+
					'</a>'+
				'</caption>' +

				'<table class="table table-hover table-striped table-bordered supotable-child-table tableclass_'+d.id+'"' +
						'style="border-radius:4px;overflow:hidden;background-color:white;">' + 
					'<thead>' + 
						'<tr class="danger">'+ 
							'<td width="40%"><strong>Part SKU</strong></td>'+ 
							'<td width="30%"><strong>Type</strong></td>'+ 
							'<td width="25%"><strong>Qty</strong></td>'+ 
							'<td width="5%"></td>'+ 
						'</tr>' +
		        	'</thead>'+ 
	        	'<tbody>';
		

	    //Special Orders
		if (typeof spo !== "undefined") {
			for (var i = spo.length - 1; i >= 0; i--) {

				html += "" + 
					"<tr>"+
						"<td><a href='/parts/public/specialorders/"+spo[i].id+"/edit'>" + d.partsku + "</a></td>"+
						"<td>Special Order</td>"+
						"<td>" + spo[i].qty + "</td>"+
						"<td width='3%' class='text-center'>"+
							"<label>"+
								"<input type='checkbox' value='spo' data-qty='"+spo[i].qty+"' data-soid='"+d.id+"' data-spoid='"+spo[i].id+"'  class='procb chr"+d.id+" item"+spo[i].id+"' "+willTheItemBeChecked(spo[i].id, d.spogo)+">"+
							"</label>"+
						"</td>"+
					"</tr>";
			}
		}


		//Back Orders
		if (typeof bko !== "undefined") {
			for (var i = bko.length - 1; i >= 0; i--) {
				html += "" + 
					"<tr>"+
						"<td><a href='/parts/public/backorders/"+bko[i].id+"/edit'>" + d.partsku + "</a></td>"+
						"<td>Back Orders</td>"+
						"<td>" + bko[i].qty + "</td>"+
						"<td width='3%' class='text-center'>"+
							"<label>"+
								"<input type='checkbox' value='bko' data-qty='"+bko[i].qty+"' data-soid='"+d.id+"' data-bkoid='"+bko[i].id+"'  class='procb chr"+d.id+" item"+bko[i].id+"' "+willTheItemBeChecked(bko[i].id, d.bkogo)+">"+
							"</label>"+
						"</td>"+
					"</tr>";
			}
		}

		//Openboxes
		if (typeof opb !== "undefined") {
			for (var i = opb.length - 1; i >= 0; i--) {
				html += "" + 
					"<tr>"+
						"<td><a href='/parts/public/openboxes/"+opb[i].id+"/edit'>" + d.partsku + "</a></td>"+
						"<td>Open Boxes</td>"+
						"<td>" + opb[i].qty + "</td>"+
						"<td width='3%' class='text-center'>"+
							"<label>"+
								"<input type='checkbox' value='opb' data-qty='"+opb[i].qty+"' data-soid='"+d.id+"' data-opbid='"+opb[i].id+"'  class='procb chr"+d.id+" item"+opb[i].id+"' "+willTheItemBeChecked(opb[i].id, d.opbgo)+">"+
							"</label>"+
						"</td>"+
					"</tr>";
			}
		}


		//RAPS
		if (typeof rap !== "undefined") {
			for (var i = rap.length - 1; i >= 0; i--) {
				html += "" + 
					"<tr>"+
						"<td><a href='/parts/public/raps/"+rap[i].id+"/edit'>" + d.partsku + "</a></td>"+
						"<td>RAPs</td>"+
						"<td>" + rap[i].qty + "</td>"+
						"<td width='3%' class='text-center'>"+
							"<label>"+
								"<input type='checkbox' value='rap' data-qty='"+rap[i].qty+"' data-soid='"+d.id+"' data-rapid='"+rap[i].id+"'  class='procb chr"+d.id+" item"+rap[i].id+"' "+willTheItemBeChecked(rap[i].id, d.rapgo)+">"+
							"</label>"+
						"</td>"+
					"</tr>";
			}
		}



		//Needs Ordering
		if (typeof no !== "undefined") {
			//console.log(no);
			for (var i = no.length - 1; i >= 0; i--) {
				if (no[i].sys_iscurrent == "1") {
					reorderqty = no[i].reorderqty;
					orderqty = d.orderqty;

					checked = (d.pigo == "1" ? "checked='true'" : "");
					html += "" +
						"<tr>"+
							"<td><a href='/parts/public/partspms/"+no[i].partsku_id+"/edit'>" + d.partsku + "</a></td>"+
							"<td>Needs Ordering</td>"+
							"<td>" + 
								"<a href='javascript:void(0)' class='supotable-popup' id='aid"+d.id+"_"+no[i].id+"' data-toggle='popover' data-soid='"+d.id+"' data-noid='"+no[i].id+"' data-reorderqty='" + no[i].reorderqty + "' data-orderqty='" + orderqty + "'>" + orderqty + "</a>" +
							"</td>"+
							"<td width='3%' class='text-center'>"+
								"<label>"+
									"<input type='checkbox' value='no' data-soid='"+d.id+"' data-noid='"+no[i].id+"' data-orderqty='"+orderqty+"' class='procb chr"+d.id+" item"+no[i].id+"' "+checked+" >"+
								"</label>"+
							"</td>"+
						"</tr>";
				}
			}
		}
		
		html += "" + '</tbody>' + '</table>' + '</div>';

		return html;
	}

	function willTheItemBeChecked(id, str){
		if (str != "0") {
			arr = str.split(";");
			let obj = arr.find(item => item.toString() == id.toString());
			let index = arr.indexOf(obj);
			if (index >= 0) {
				return "checked='true'";
			}
		}
		return "";
	}

	
	function addIdToString(id, str){
		
		if (str != "0") {
			arr = str.split(";");
			let obj = arr.find(item => item.toString() == id.toString());
			let index = arr.indexOf(obj);
			if (index < 0) {
				arr = arr.filter(item => item.toString() != "");
				arr.push(id.toString());
				str = arr.join(";");
			}
		}else{
			str = id + ";";
		}
		return str;
	}

	function removeIdFromString(id, str){
		if (str != "0") {
			str = str.split(";").filter(item => (item.toString() != id.toString())).join(";");
		}

		return (str == "" ? "0" : str);
	}

	function checkOrNotThisCB(soid, id, typ, toCheck){
		if (toCheck == 1) {
			$(".chr"+soid+".item"+id+"[value='"+typ+"']").prop('checked', true);	
		}else{
			$(".chr"+soid+".item"+id+"[value='"+typ+"']").prop('checked', false).removeAttr('checked');
		}
		
		console.log("Checked/Removed Check");
	}

	function groupCheckOrNotThisCB(soid, toCheck){
		if (toCheck == 1) {
			$(".chr"+soid).prop('checked', true);	
		}else{
			$(".chr"+soid).prop('checked', false).removeAttr('checked');
		}
		
		console.log("Group Checked/Removed Check");
	}


	$(function(){
		

		modifyEdit = function(soid, noid, qty){
			theOrderQty = $("#supotable-popup-input").val();
			if (theOrderQty <= qty) {
				$("#aid"+soid+"_"+noid).data("orderqty", theOrderQty).text(theOrderQty);	

				theCheckbox = $(".chr"+soid+".item"+noid+"[value='no']");
				oldValue = theCheckbox.data("orderqty");
				//alert(oldValue);
				theCheckbox.data("orderqty", theOrderQty);
				
				var d = table.row('.shown').data();

				
				d.orderqty = theOrderQty.toString();
				if (theCheckbox.prop("checked")) { //if the checkbox is checked
			
					checkOrNotThisCB(soid, noid, "no", 2);
					//d.orderqty = d.reorderqty;

					d.pigo="0";
					d.totalorders = parseInt(d.totalorders) - parseInt(oldValue);
				}
				table.row('.shown').data(d).draw();
			}
			destroyPopover();
		}

		
		$(".navmenuitemlist li.dropdown").removeClass('active').eq(1).addClass('active');
        $(".navsubmenuitemlist li").removeClass('active').eq(5).addClass('active');

		$('#supotable').popover({
            selector: '[class="supotable-popup"]',
            content: 
            		"<input type='number' min='1' class='form-control input-sm' id='supotable-popup-input'>&nbsp;"+
            		"<button class='btn btn-success btn-sm' id='supotable-popup-button-yes' onclick=''><i class='fa fa-check'></i></button>&nbsp;"+
            		"<button class='btn btn-danger btn-sm' id='supotable-popup-button-no' onclick='javascript:destroyPopover()'><i class='fa fa-times'></i></button>",
            html: true,
            placement: 'top',
            trigger: 'click'
        });

        $('#supotable').on('shown.bs.popover', '.supotable-popup', function(event) {
        	event.preventDefault();
        	theSource = $(this);
        	reorderqty = theSource.data("reorderqty");
        	orderqty = theSource.data("orderqty");
        	noid = theSource.data("noid");
        	soid = theSource.data("soid");

        	theInputBox = $("#supotable-popup-input");
        	theInputBox.val(orderqty);
        	theInputBox.focus();

        	var d = table.row('.shown').data();

        	$("#supotable-popup-button-yes").attr("onclick", "javascript:modifyEdit("+soid+", "+noid+", "+reorderqty+");");
        });

        

		var table = $('#supotable')
			.on( 'init.dt', function () {
		        $("#supotable tbody").removeClass('hidden');
		        $("#loadersuppo").addClass('hidden');
		    })
			.DataTable({
				"ajax": '/parts/public/json/supplierordersjson.json',
				"columns": [
			            {
			                "className":'details-control',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ''
			            }, //0
			            { "data": "created_at" }, //1
			            { "data": "parentcode" }, //2
			            { "data": "suppcode" }, //3
			            { "data": "partsku" }, //4
			            { "data": "reorderqty" }, //5
			            { "data": "backorder" }, //6
			            { "data": "openbox" }, //7
			            { "data": "specialorder" }, //8
			            { "data": "rap" }, //9
			            { "data": "sys_total" }, //10
			            { "data": "descr" }, //11

			            { "data": "id" }, //12
			            { "data": "sys_finalstatus" }, //13

			            { "data": "orderqty" }, //14
			            { "data": "pigo" }, //15
			            { "data": "bkogo" }, //16
			            { "data": "opbgo" }, //17
			            { "data": "spogo" }, //18
			            { "data": "rapgo" }, //19

			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"stocksid",
			            }, //20 - PIs
			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"backordersid",
			            }, //21 - BKOs
			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"openboxesid",
			            }, //22 - OBPs
			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"specialordersid",
			            }, //23 - SPOs
			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"rapsid",
			            }, //24 - RAPs
			            { "data": "totalorders" }, //25 - total orders
			            { 
			            	"className": 'select-checkbox',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ''
			            } //last
		            ],
		        "columnDefs": [
			        	{
			            	"targets": [1],
			            	"render": function ( data, type, full, meta ) {
			            		var d = new Date(data);

								return formatThisDate(d);
						    }
			            },
			            {
			                "targets": [ 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,24, 25  ],//14, 15, 16, 17, 18, 19
			                "visible": false,
			                "searchable": true
			            },
		        	],
		        "select": {
		        	style: 'multi',
		        	selector: 'td:last-child'
		        },
		        dom: 'Bfrtip',
	       		"buttons": [
	       				{
				            extend: 'csv',
				            text: '<i class="fa fa-download"></i> Download CSV',
				            className: 'btn-primary',
				            title: 'SupplierOrders_Export'
				        },
			            {
				            extend: 'selected',
				            text: '<i class="fa fa-sign-in"></i> Proceed with Order',
				            className: 'btn-primary',
				            action: function ( e, dt, button, config ) {
				                //alert( dt.rows( { selected: true } ).indexes().length +' row(s) selected' );
				                
				                $("#so-checked").val(JSON.stringify(dt.rows({selected: true}).data().toArray()));
			                	$("#supplierordersform").submit();
				                

				            }
				        },
				        	

			        ],
	            "order": [[1, 'desc']],
	            
	        	"deferRender": true

		});
		table.buttons().container().appendTo( $('#buttoncontainer') );

		table.on( 'select', function ( e, dt, type, indexes ) {
		    if ( type === 'row' ) {
		    	var totalqty = $("#totalOrderQty");
		    	var data = table.rows(indexes).data();
		    	var d = table.row(indexes).data();
		    	
		    	var so = data.pluck("id");
		    	var px = data.pluck( 'stocksid' );
		    	var spx = data.pluck( 'specialordersid' );
		    	var bkx = data.pluck("backordersid");
		    	var opbx = data.pluck("openboxesid");
		    	var rapx = data.pluck("rapsid");

		    	d.totalorders = 0;
		    	table.row(indexes).data(d).draw();


		    	if (px[0].length > 0) {
		 			d.pigo = "1";
		 			d.totalorders += parseInt(d.orderqty);
		 			table.row(indexes).data(d).draw();
		 			
		 			//checkOrNotThisCB(so[0], px[0][0], "no", 1);
		 		}

		    	if (spx[0].length > 0) {
		 			for (var i = spx[0].length - 1; i >= 0; i--) {
		 				d.spogo = addIdToString(spx[0][i].id, d.spogo);
		 				d.totalorders += parseInt(spx[0][i].qty);
		 				table.row(indexes).data(d).draw();
		 			}
		 		}

		 		if (bkx[0].length > 0) {
		 			for (var i = bkx[0].length - 1; i >= 0; i--) {
		 				d.bkogo = addIdToString(bkx[0][i].id, d.bkogo);
		 				d.totalorders += parseInt(bkx[0][i].qty);
		 				table.row(indexes).data(d).draw();
		 				
		 			}
		 		}

		 		if (opbx[0].length > 0) {
		 			for (var i = opbx[0].length - 1; i >= 0; i--) {
		 				d.opbgo = addIdToString(opbx[0][i].id, d.opbgo);
		 				d.totalorders += parseInt(opbx[0][i].qty);
		 				table.row(indexes).data(d).draw();
		 				
		 			}
		 		}

		 		if (rapx[0].length > 0) {
		 			for (var i = rapx[0].length - 1; i >= 0; i--) {
		 				d.rapgo = addIdToString(rapx[0][i].id, d.rapgo);
		 				d.totalorders += parseInt(rapx[0][i].qty);
		 				table.row(indexes).data(d).draw();
		 				
		 			}
		 		}

		 		//totalqty.val(parseInt(totalqty.val()) + parseInt(d.sys_total));
		 		groupCheckOrNotThisCB(so[0], 1);
		    }
		} );

		table.on( 'deselect', function ( e, dt, type, indexes ) {
		    if ( type === 'row' ) {

		    	var data = table.rows(indexes).data();
		    	var d = table.row(indexes).data();
		    	
		    	var so = data.pluck("id");
		    	
		    	d.pigo = "0"; d.bkogo = "0"; d.opbgo = "0"; d.spogo = "0"; d.rapgo = "0";
		    	d.totalorders = 0;
		    	table.row(indexes).data(d).draw();
		    	groupCheckOrNotThisCB(so[0], 2);

		    }
		} );

		$("#supotable_filter").closest("div").addClass('text-right');
		$("#supotable_wrapper").find("div.row").first().addClass('text-left');


		$('#supotable tbody').on('click', 'td.details-control', function () {
		    var tr = $(this).closest('tr');
		    var row = table.row( tr );
		 
		    if ( row.child.isShown() ) {
		        // This row is already open - close it
		        row.child.hide();
		        tr.removeClass('shown');

		    }
		    else {
		        if ( table.row( '.shown' ).length ) {
					$('.details-control', table.row( '.shown' ).node()).click();
				}

				row.child( format(row.data()) ).show();
				tr.addClass('shown');
		    }
		});


		$("#supotable tbody").on('change', '.procb', function(event) {
			event.preventDefault();

			var d = table.row('.shown').data();

			cname = $("."+this.className.split(" ")[1]);
			len = cname.length; countOfChecked = 0;

			cname.each(function(index, el) {
				if (el.checked) {
					countOfChecked++;
				}
			});
			
			if (this.checked) {
				tdText = this.value;
				switch(tdText){
					case 'spo':
						thisSoid = $(this).data("soid");
						thisSpoid = $(this).data("spoid");

						d.spogo = addIdToString(thisSpoid, d.spogo);
						table.row('.shown').data(d).draw();

						break;
					case 'no':
						d.pigo = "1";
						d.orderqty = $(this).data("orderqty");
						
						table.row('.shown').data(d).draw();
						break;

					case 'bko':
						thisSoid = $(this).data("soid");
						thisBkoid = $(this).data("bkoid");
						
						d.bkogo = addIdToString(thisBkoid, d.bkogo);
						table.row('.shown').data(d).draw();

						break;

					case 'opb':
						thisSoid = $(this).data("soid");
						thisOpbId = $(this).data("opbid");
						
						d.opbgo = addIdToString(thisOpbId, d.opbgo);
						table.row('.shown').data(d).draw();

						break;

					case 'rap':
						thisSoid = $(this).data("soid");
						thisRapId = $(this).data("rapid");
						
						d.rapgo = addIdToString(thisRapId, d.rapgo);
						table.row('.shown').data(d).draw();

						break;

				}

				if (countOfChecked == len) {
					d.totalorders = 0;
					table.row('.shown').data(d).draw();
					table.row('.shown', { page: 'current' }).select();
				}else{
					if (tdText != "no") {
						d.totalorders += $(this).data("qty");
						table.row('.shown').data(d).draw();
					}else{
						d.totalorders += parseInt($(this).data("orderqty"));
						table.row('.shown').data(d).draw();
					}

				}

			}else{ // IF UNCHECKED
				tdText = this.value;
				switch(tdText){
					case 'spo':
						thisSoid = $(this).data("soid");
						thisSpoid = $(this).data("spoid");

						d.spogo = removeIdFromString(thisSpoid, d.spogo);
						table.row('.shown').data(d).draw();
						break;

					case 'no':
						d.pigo = "0";
						table.row('.shown').data(d).draw();
						break;

					case 'bko':
						thisSoid = $(this).data("soid");
						thisBkoid = $(this).data("bkoid");

						d.bkogo = removeIdFromString(thisBkoid, d.bkogo);
						table.row('.shown').data(d).draw();
						break;

					case 'opb':
						thisSoid = $(this).data("soid");
						thisOpbid = $(this).data("opbid");

						d.opbgo = removeIdFromString(thisOpbid, d.opbgo);
						table.row('.shown').data(d).draw();
						break;

					case 'rap':
						thisSoid = $(this).data("soid");
						thisRapid = $(this).data("rapid");

						d.rapgo = removeIdFromString(thisRapid, d.rapgo);
						table.row('.shown').data(d).draw();
						break;
				}

				if (countOfChecked==0) {
					table.row('.shown', { page: 'current' }).deselect();
				}else{
					if (tdText != "no") {
						d.totalorders -= $(this).data("qty");
						table.row('.shown').data(d).draw();
					}else{
						d.totalorders -= parseInt($(this).data("orderqty"));
						table.row('.shown').data(d).draw();
					}
				}

			}


		});

		



	});